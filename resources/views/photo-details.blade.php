<div class="container-fluid" style="background-color:#EFEFEF;">
       
       <div class="container margin-top10">
           <div class="row">
               <div class="col-sm-8">
                    <div class="row top-head">
                      <h1>You Can View Photo From Here - </h1>
                    </div> 

                    <div class="row">
                      <div class="col-lg-3 col-sm-4 col-xs-6" style="padding: 0"><a title="Image 1" href="#"><img class="thumbnail img-responsive" src="{{asset('frontend/images/school-image2.jpg')}}"></a></div>
                      <div class="col-lg-3 col-sm-4 col-xs-6" style="padding: 0"><a title="Image 2" href="#"><img class="thumbnail img-responsive" src="{{asset('frontend/images/school-image2.jpg')}}"></a></div>
                      <div class="col-lg-3 col-sm-4 col-xs-6" style="padding: 0"><a title="Image 3" href="#"><img class="thumbnail img-responsive" src="{{asset('frontend/images/school-image2.jpg')}}"></a></div>
                      <div class="col-lg-3 col-sm-4 col-xs-6" style="padding: 0"><a title="Image 4" href="#"><img class="thumbnail img-responsive" src="{{asset('frontend/images/school-image2.jpg')}}"></a></div>
                      <div class="col-lg-3 col-sm-4 col-xs-6" style="padding: 0"><a title="Image 5" href="#"><img class="thumbnail img-responsive" src="{{asset('frontend/images/school-image2.jpg')}}"></a></div>
                      <div class="col-lg-3 col-sm-4 col-xs-6" style="padding: 0"><a title="Image 6" href="#"><img class="thumbnail img-responsive" src="{{asset('frontend/images/school-image2.jpg')}}"></a></div>
                      <div class="col-lg-3 col-sm-4 col-xs-6" style="padding: 0"><a title="Image 8" href="#"><img class="thumbnail img-responsive" src="{{asset('frontend/images/school-image2.jpg')}}"></a></div>
                      <div class="col-lg-3 col-sm-4 col-xs-6" style="padding: 0"><a title="Image 9" href="#"><img class="thumbnail img-responsive" src="{{asset('frontend/images/school-image2.jpg')}}"></a></div>
                      <div class="col-lg-3 col-sm-4 col-xs-6" style="padding: 0"><a title="Image 10" href="#"><img class="thumbnail img-responsive" src="{{asset('frontend/images/school-image2.jpg')}}"></a></div>
                      <div class="col-lg-3 col-sm-4 col-xs-6" style="padding: 0"><a title="Image 11" href="#"><img class="thumbnail img-responsive" src="{{asset('frontend/images/school-image2.jpg')}}"></a></div>
                      <div class="col-lg-3 col-sm-4 col-xs-6" style="padding: 0"><a title="Image 12" href="#"><img class="thumbnail img-responsive" src="{{asset('frontend/images/school-image2.jpg')}}"></a></div>
                      <div class="col-lg-3 col-sm-4 col-xs-6" style="padding: 0"><a title="Image 13" href="#"><img class="thumbnail img-responsive" src="{{asset('frontend/images/school-image2.jpg')}}"></a></div>
                    </div>

                    <div class="modal" id="myModal" role="dialog">
                      <div class="modal-dialog">
                      <div class="modal-content">
                      <div class="modal-header">
                        <button class="close" type="button" data-dismiss="modal">×</button>
                        <h3 class="modal-title"></h3>
                      </div>
                      <div class="modal-body">
                        <div id="modalCarousel" class="carousel">
                              <div class="carousel-inner"></div>
                              <a class="carousel-control left" href="#modaCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                              <a class="carousel-control right" href="#modalCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                            </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                       </div>
                      </div>      
                    </div>
               </div>
               <div class="col-sm-4 float-right">
                    <div class="col-xs-12 col-sm-12">
                        <div class="row top-head-right">
                            <a href="#"><i class="fa fa-home" aria-hidden="true"></i></a> / Album / Photo
                        </div>

                        <div class="row right-video">
                            <i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video
                        </div>
                        <div class="row margin-bottom20">
                            <iframe width="360" height="195" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe>
                        </div>

                         <div class="row related-topics box-shado margin-bottom12p">
                            <a href="video-gallery.php"><img src="{{asset('frontend/images/videogallery-ico.png')}}" class="img-responsive"></a>
                        </div>

                    </div>
                </div>
           </div>
       </div>  
    </div>