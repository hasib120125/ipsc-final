@include('include.header')

<div class="container-fluid body-bg">
    <div class="container margin-top10">
        <div class="row">
            <div class="col-sm-8">
              <div class="row top-head box-shado">
                  <div class="col-xs-5 col-sm-7"><h1 class="notice-head">IPSC  NOTICE</h1></div>
                  <form name="notice" action="#" method="post">
                    <div class="col-xs-4 col-sm-3">
                      <select name="cmb_cat" class="form-control">
                          <option value="0">All Notice</option>
                          <option value="1">XI Class ( Morning- Scien</option>
                          <option value="2">XI Class</option>
                          <option value="3">Admitted Students List</option>
                          <option value="4">Office</option>
                          <option value="5">College</option>
                          <option value="6">School</option>
                      </select>
                    </div>
                    <div class="col-xs-3 col-sm-2"><input type="submit" name="search" value="Search" class="form-control btn-primary pull-right"></div>
                  </form>
              </div> 

              <div class="row box-shado margin-bottom10">
                   <div class="col-xs-2 col-sm-2 notice-date" align="center">
                       <span>10 <br> Feb <br> 2018</span>
                   </div>
                   <div class="col-xs-8 col-sm-9 div-title">
                      <span class="notice-title">Recruitment Notice - 2018</span><br>
                      <span class="views-txt">Notice Department :
                           Office                       
                      </span><br>
                       <span class="views-txt">Total Views : 381 Views</span>
                   </div>
                   <div class="col-xs-2 col-sm-1 div-icon">
                       <div class="col-sm-12"><a href="#" target="_blank" "=""><i class="fa fa-eye z-index1000" aria-hidden="true"></i></a></div>
                       <div class="col-sm-12"><a href="https://www.rhodeshouse.ox.ac.uk/media/1002/sample-pdf-file.pdf"><i class="fa fa-cloud-download" aria-hidden="true"></i></a></div>
                   </div>
                </div>

                <div class="row box-shado margin-bottom10">
                   <div class="col-xs-2 col-sm-2 notice-date" align="center">
                       <span>10 <br> Feb <br> 2018</span>
                   </div>
                   <div class="col-xs-8 col-sm-9 div-title">
                      <span class="notice-title">Recruitment Notice - 2018</span><br>
                      <span class="views-txt">Notice Department :
                           Office                       
                      </span><br>
                       <span class="views-txt">Total Views : 381 Views</span>
                   </div>
                   <div class="col-xs-2 col-sm-1 div-icon">
                       <div class="col-sm-12"><a href="#" target="_blank" "=""><i class="fa fa-eye z-index1000" aria-hidden="true"></i></a></div>
                       <div class="col-sm-12"><a href="https://www.rhodeshouse.ox.ac.uk/media/1002/sample-pdf-file.pdf"><i class="fa fa-cloud-download" aria-hidden="true"></i></a></div>
                   </div>
                </div>

                <div class="row box-shado margin-bottom10">
                   <div class="col-xs-2 col-sm-2 notice-date" align="center">
                       <span>10 <br> Feb <br> 2018</span>
                   </div>
                   <div class="col-xs-8 col-sm-9 div-title">
                      <span class="notice-title">Recruitment Notice - 2018</span><br>
                      <span class="views-txt">Notice Department :
                           Office                       
                      </span><br>
                       <span class="views-txt">Total Views : 381 Views</span>
                   </div>
                   <div class="col-xs-2 col-sm-1 div-icon">
                       <div class="col-sm-12"><a href="#" target="_blank" "=""><i class="fa fa-eye z-index1000" aria-hidden="true"></i></a></div>
                       <div class="col-sm-12"><a href="https://www.rhodeshouse.ox.ac.uk/media/1002/sample-pdf-file.pdf"><i class="fa fa-cloud-download" aria-hidden="true"></i></a></div>
                   </div>
                </div>

            </div>

            <div class="col-sm-4">
                <div class="col-xs-12 col-sm-12">
                    <div class="row top-head-right box-shado">
                        <a href="#"><i class="fa fa-home" aria-hidden="true"></i></a> Academic / Notice
                    </div>

                    <div class="row right-video box-shado">
                        <i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video
                    </div>
                    <div class="row margin-bottom20">
                        <iframe width="360" height="195" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe>
                    </div>

                    <div class="row related-topics box-shado">
                        <i class="fa fa-windows" aria-hidden="true"></i> Related Topics
                    </div>
                    <div class="row margin-bottom20">
                        @include("aside/academic-aside")
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


@include('include.footer')