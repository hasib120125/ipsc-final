@include('include.header')


<div class="container-fluid body-bg">
    <div class="container margin-top10">
        <div class="row">
            <div class="col-sm-8" style="margin-bottom: 10px;">
                <div class="row top-head">
                    <h1>Ispahani Public School & College Messages:</h1>
                </div>

                <div class="col-sm-12 body-message">
                    <img src="{{asset('frontend/images/vc.png')}}" class="img-responsive margin-bottom2P margin-right10" alt="" title="" align="left">
                </div>
                <br>

                <div class="col-sm-12" style="font-size: 16px;text-align: justify;">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                    &nbsp;
                    <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets</p>

                    &nbsp;

                    <p>containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

                    &nbsp;

                    <h4>Designation</h4>
                    <p>Name</p>
                </div>

            </div>

            <div class="col-sm-4">
                <div class="col-xs-12 col-sm-12">
                    <div class="row top-head-right">
                        <a href="#"><i class="fa fa-home" aria-hidden="true"></i></a> / Administration / Messages
                    </div>

                    <div class="row right-video">
                        <i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video
                    </div>
                    <div class="row margin-bottom20">
                        <iframe width="360" height="195" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe>
                    </div>

                    <div class="row related-topics box-shado">
                        <i class="fa fa-windows" aria-hidden="true"></i> Related Topics
                    </div>
                    <div class="row margin-bottom20">
                        @include("aside/administration-aside")
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@include('include.footer')