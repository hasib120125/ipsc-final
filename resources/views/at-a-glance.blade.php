@include('include.header')

<div class="container-fluid body-bg">
	<div class="container margin-top10">
		<div class="row">
			<div class="col-sm-8">
                <div class="row top-head">
                    <h1>Ispahani Public School & College Details</h1>
                </div>

                <div class="row body-container">
                    <img src="{{asset('frontend/images/collage.png')}}" class="img-responsive margin-bottom2P" alt="Birshrestha Munshi Abdur Rouf Public College  At a Glance" title="Birshrestha Munshi Abdur Rouf Public College  At a Glance">      
                    <p>The organization, named Cantonment Public School, started on 28 September 1962 and started its education program at present Comilla Zilla School. In 1963, the school was shifted to the current Cantonment Board School. Initially, SSC examination was conducted in this school in 1964. It was named Mainamati Public School in June 1965. On September 28, 1966, after the financial assistance of donor Mirza Ahmed Ispahani, the name of the company was named Ispahani Public School. After the establishment of the college branch on October 28, 1975, the institute was renamed as Ispahani Public School and College.</p>

                    <p>&nbsp;</p>

                    <p>At the outset the number of students was only 150, while the number of teachers was only 8 including the Headmaster and the Assistant Headmaster. It is needless to say that there was no infrastructure that can be mentioned here.</p>

                    <p>&nbsp;</p>

                    <p><strong>Government’s approval and recognition:</strong><br>
                    At first this institution got recognition as a Junior High School. Within a period of one year it turned into a High School on January 01, 1985. Only 23 students (Science and Humanities) appeared at the Secondary School Certificate (SSC) Examination in 1987 for the first time from this institution.</p>

                    <p>&nbsp;</p>

                    <p><strong>Opening of the College Section:</strong><br>
                    The organization, named Cantonment Public School, started on 28 September 1962 and started its education program at present Comilla Zilla School. In 1963, the school was shifted to the current Cantonment Board School. Initially, SSC examination was conducted in this school in 1964. It was named Mainamati Public School in June 1965. On September 28, 1966, after the financial assistance of donor Mirza Ahmed Ispahani, the name of the company was named Ispahani Public School. After the establishment of the college branch on October 28, 1975, the institute was renamed as Ispahani Public School and College.</p>

                    <p>&nbsp;</p>

                    <p><strong>Morning Shift Opening:</strong><br>
                    Later on the then Director General of Bangladesh Rifles, Major General Ejaj Ahmed Chowdhury inaugurated the Morning Shift on March 20, 1995.</p>

                    <p>&nbsp;</p>

                    <p><strong>Evening Shift:</strong><br>
                    In 1963, the school was shifted to the current Cantonment Board School. Initially, SSC examination was conducted in this school in 1964. It was named Mainamati Public School in June 1965. On September 28, 1966, after the financial assistance of donor Mirza Ahmed Ispahani, the name of the company was named Ispahani Public School. After the establishment of the college branch on October 28, 1975, the institute was renamed as Ispahani Public School and College.</p>

                    <p>&nbsp;</p>
                
                    <p>&nbsp;</p>
                </div>
                <div class="col-sm-12" style="margin-top: 10px">
                    Total Visitor : 1400
                </div>
            </div>
			<div class="col-sm-4">
				<div class="col-xs-12 col-sm-12">
                    <div class="row top-head-right">
                        <a href="#"><i class="fa fa-home" aria-hidden="true"></i></a> / <a href="#">  Details</a>
                    </div>

                    <div class="row right-video">
                        <i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video
                    </div>
                    <div class="row margin-bottom20">
                        <iframe width="360" height="195" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe>
                    </div>

                    <div class="row related-topics box-shado">
                        <i class="fa fa-windows" aria-hidden="true"></i> Related Topics
                    </div>
                    <div class="row margin-bottom20">
                        @include("aside/about-us-aside")
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>

@include('include.footer')