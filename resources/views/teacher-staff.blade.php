@include('include.header')

<div class="container-fluid body-bg">
    <div class="container margin-top10">
        <div class="row">
            <div class="col-sm-8">
                <div class="row top-head box-shado">
                    <h1> Teacher</h1>
                </div>

                <div class="row body-container">
                    <div class="col-xs-3 col-sm-3 padding-left0">
                        <img src="{{asset('frontend/images/vc.png')}}" class="img-responsive" alt="" title="">
                    </div>
                    <div class="col-xs-9 col-sm-9">
                        <div class="row"><div class="col-sm-12 techer-name">Teacher Name</div></div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 deg-text">Designation</div>
                            <div class="col-xs-1 col-sm-1"> : </div>
                            <div class="col-sm-8">Designation</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 deg-text">Department</div>
                            <div class="col-xs-1 col-sm-1"> : </div>
                            <div class="col-sm-8">
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 deg-text">Education</div>
                            <div class="col-xs-1 col-sm-1"> : </div>
                            <div class="col-sm-8">Education</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 deg-text">Joining</div>
                            <div class="col-xs-1 col-sm-1"> : </div>
                            <div class="col-sm-8">JoinDate</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 deg-text">ID</div>
                            <div class="col-xs-1 col-sm-1"> : </div>
                            <div class="col-sm-8">StaffID</div>
                        </div>
                        <div class="row"><div class="col-sm-12 deg-text">View Full Profile</div></div>
                    </div>
                </div>

                <div class="row body-container">
                    <div class="col-xs-3 col-sm-3 padding-left0">
                        <img src="{{asset('frontend/images/vc.png')}}" class="img-responsive" alt="" title="">
                    </div>
                    <div class="col-xs-9 col-sm-9">
                        <div class="row"><div class="col-sm-12 techer-name">Teacher Name</div></div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 deg-text">Designation</div>
                            <div class="col-xs-1 col-sm-1"> : </div>
                            <div class="col-sm-8">Designation</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 deg-text">Department</div>
                            <div class="col-xs-1 col-sm-1"> : </div>
                            <div class="col-sm-8">
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 deg-text">Education</div>
                            <div class="col-xs-1 col-sm-1"> : </div>
                            <div class="col-sm-8">Education</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 deg-text">Joining</div>
                            <div class="col-xs-1 col-sm-1"> : </div>
                            <div class="col-sm-8">JoinDate</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 deg-text">ID</div>
                            <div class="col-xs-1 col-sm-1"> : </div>
                            <div class="col-sm-8">StaffID</div>
                        </div>
                        <div class="row"><div class="col-sm-12 deg-text">View Full Profile</div></div>
                    </div>
                </div>
                
            </div>

            <div class="col-sm-4">
                <div class="col-xs-12 col-sm-12">
                    <div class="row top-head-right box-shado">
                        <a href="#"><i class="fa fa-home" aria-hidden="true"></i></a> / Administration / Staff
                    </div>

                    <div class="row right-video box-shado">
                        <i class="fa fa-file-video-o" aria-hidden="true"></i> College Video
                    </div>
                    <div class="row margin-bottom20">
                        <iframe width="360" height="195" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe>
                    </div>

                    <div class="row right-video box-shado">
                        <i class="fa fa-windows" aria-hidden="true""></i> Department
                    </div>
                    <div class="row box-shado margin-bottom20">
                        <ul class="list-unstyled">
                            
                        </ul>
                    </div>

                    <div class="row related-topics box-shado">
                        <i class="fa fa-windows" aria-hidden="true"></i> Related Topics
                    </div>
                    <div class="row margin-bottom20">
                        @include("aside/administration-aside")
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



@include('include.footer')