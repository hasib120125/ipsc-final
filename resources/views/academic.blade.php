@include('include.header')

<div class="container-fluid body-bg">
	<div class="container margin-top10">
	<div class="row">
		<div class="col-sm-8">
			<div class="row top-head box-shado">
                <h1>Ispahani Public School & College Academic</h1>
            </div>

            <div class="row box-shado down-bg">
                <div class="col-xs-2 col-sm-2 down-pdf" align="center"><a href="#" target="_blank" "=""><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></div>
                <div class="col-xs-8 col-sm-9">
                    <h2>Academic Calender</h2>
                    <span>Department :

                    </span><br>
                    <span>Date : Jun 24, 2016</span><br>
                    <span>Total Views : 651 views</span>
                </div>
                <div class="col-xs-2 col-sm-1 padding-top10p">
                    <div class="col-sm-12"><a href="#"><i class="fa fa-cloud-download" aria-hidden="true"></i></a></div>
                </div>
            </div>

            <div class="row box-shado down-bg">
                <div class="col-xs-2 col-sm-2 down-pdf" align="center"><a href="#" target="_blank" "=""><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></div>
                <div class="col-xs-8 col-sm-9">
                    <h2>Academic Calender</h2>
                    <span>Department :

                    </span><br>
                    <span>Date : Jun 24, 2016</span><br>
                    <span>Total Views : 651 views</span>
                </div>
                <div class="col-xs-2 col-sm-1 padding-top10p">
                    <div class="col-sm-12"><a href="#"><i class="fa fa-cloud-download" aria-hidden="true"></i></a></div>
                </div>
            </div>

            <div class="row box-shado down-bg">
                <div class="col-xs-2 col-sm-2 down-pdf" align="center"><a href="#" target="_blank" "=""><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></div>
                <div class="col-xs-8 col-sm-9">
                    <h2>Academic Calender</h2>
                    <span>Department :

                    </span><br>
                    <span>Date : Jun 24, 2016</span><br>
                    <span>Total Views : 651 views</span>
                </div>
                <div class="col-xs-2 col-sm-1 padding-top10p">
                    <div class="col-sm-12"><a href="#"><i class="fa fa-cloud-download" aria-hidden="true"></i></a></div>
                </div>
            </div>
		</div>

		<div class="col-sm-4">
			<div class="col-xs-12 col-sm-12">
                <div class="row top-head-right box-shado">
                    <a href="#"><i class="fa fa-home" aria-hidden="true"></i></a> / Academic /
                </div>

                <div class="row right-video box-shado">
                    <i class="fa fa-file-video-o" aria-hidden="true"></i> College Video
                </div>
                <div class="row margin-bottom20">
                    <iframe width="360" height="195" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe>
                </div>

                <div class="row related-topics box-shado">
                    <i class="fa fa-windows" aria-hidden="true"></i> Related Topics
                </div>
                <div class="row margin-bottom20">
                   @include("aside/academic-aside")
                </div>

            </div>
		</div>
	</div>
	</div>
</div>


@include('include.footer')