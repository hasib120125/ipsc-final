<div class="row margin-top5P" >
  <div class="col-xs-4 col-sm-4" style="padding-right: 25px;padding-left: 0px">
    <img src="{{asset('frontend/images/vc.png')}}" class='img-responsive img-thumbnail img-height'>
  </div>
  <div class="col-xs-8 col-sm-8 padding-left0">
    <div class="row achivement-date">Dec 5, 2015</div>
    <div class="row"><h2 class="achivement-title">7th Debating Festival of Anthropology at the University of Dhaka</h2></div>
    <div class="row achivement-brif">
       <a href='#'> Read more...</a>
    </div>
  </div>
</div>

<div class="row margin-top5P" >
  <div class="col-xs-4 col-sm-4" style="padding-right: 25px;padding-left: 0px">
    <img src="{{asset('frontend/images/vc.png')}}" class='img-responsive img-thumbnail img-height'>
  </div>
  <div class="col-xs-8 col-sm-8 padding-left0">
    <div class="row achivement-date">Dec 23, 2015</div>
    <div class="row"><h2 class="achivement-title">Royal Cement - College Debating Competition</h2></div>
    <div class="row achivement-brif">
       <a href="#"> Read more...</a>
    </div>
  </div>
</div>

<div class="row margin-top5P" >
  <div class="col-xs-4 col-sm-4" style="padding-right: 25px;padding-left: 0px">
    <img src="{{asset('frontend/images/vc.png')}}" class='img-responsive img-thumbnail img-height'>
  </div>
  <div class="col-xs-8 col-sm-8 padding-left0">
    <div class="row achivement-date">Dec 7, 2015</div>
    <div class="row"><h2 class="achivement-title">CSE Festival organized by the World University</h2></div>
    <div class="row achivement-brif">
       <a href="#"> Read more...</a>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-xs-4 col-sm-4">
  </div>
  <div class="col-xs-8 col-sm-8 padding-left0" style="text-align: right;padding-bottom: 2px;">
    <a href="#"><button type="button" class="btn-default" style="padding: 5px;width: 114px;color: #ff6b7c;">View All</button></a>
  </div>
</div>
