<div class="row notice-border">
  <div class="col-xs-3 col-sm-3">
      <div class="col-sm-12 date-month" align="center">May</div>
      <div class="col-sm-12 date-day" align="center">14</div>
  </div>
  <div class="col-xs-9 col-sm-9">
     <div class="row">
     <span class="notice-title">Class XI Fees 2017-18 </span><br>
	 <span class="notice-department">Department :</span>XI Class
  </div>
	 <div class="row padding-top5p">
	 <a href="notice_details.php" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
	 <a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
	 </div>
  </div>
</div>

<div class="row notice-border">
  <div class="col-xs-3 col-sm-3">
      <div class="col-sm-12 date-month" align="center">May</div>
      <div class="col-sm-12 date-day" align="center">9</div>
  </div>
  <div class="col-xs-9 col-sm-9">
     <div class="row">
     <span class="notice-title">Class XI Admission Circular</span><br>
	 <span class="notice-department">Department :</span>College
  </div>
	 <div class="row padding-top5p">
	 <a href="notice_details.php" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
	 <a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
	 </div>
  </div>
</div>

<div class="row notice-border">
  <div class="col-xs-3 col-sm-3">
      <div class="col-sm-12 date-month" align="center">May</div>
      <div class="col-sm-12 date-day" align="center">5</div>
  </div>
  <div class="col-xs-9 col-sm-9">
     <div class="row">
     <span class="notice-title">Recruitment Notice - 2017</span><br>
	 <span class="notice-department">Department :</span>Office
  </div>
	 <div class="row padding-top5p">
	 <a href="notice_details.php" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
	 <a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
	 </div>
  </div>
</div>

<div class="row notice-border">
  <div class="col-xs-3 col-sm-3">
      <div class="col-sm-12 date-month" align="center">Apr</div>
      <div class="col-sm-12 date-day" align="center">28</div>
  </div>
  <div class="col-xs-9 col-sm-9">
     <div class="row">
     <span class="notice-title">Recruitment Notice - 2017</span><br>
	 <span class="notice-department">Department :</span>Office
  </div>
	 <div class="row padding-top5p">
	 <a href="notice_details.php" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
	 <a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
	 </div>
  </div>
</div>

<div class="row notice-border">
  <div class="col-xs-3 col-sm-3">
      <div class="col-sm-12 date-month" align="center">Mar</div>
      <div class="col-sm-12 date-day" align="center">27</div>
  </div>
  <div class="col-xs-9 col-sm-9">
     <div class="row">
     <span class="notice-title">Principal BMARPC Facebook Page.</span><br>
	 <span class="notice-department">Department :</span>Office
  </div>
	 <div class="row padding-top5p">
	 <a href="notice_details.php" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
	 <a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
	 </div>
  </div>
</div>

<div class="row notice-border">
	<div class="col-xs-3 col-sm-3">
	    <div class="col-sm-12 date-month" align="center">Feb</div>
	    <div class="col-sm-12 date-day" align="center">5</div>
	</div>
  	<div class="col-xs-9 col-sm-9">
     	<div class="row">
     	<span class="notice-title">Class Eleven 2nd CT & Class Twelve Evaluation Exam Corrected Routine</span><br>
		<span class="notice-department">Department :</span>College
  	</div>
	<div class="row padding-top5p">
	 	<a href="notice_details.php" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
	 	<a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
	</div>
  	</div>
</div>