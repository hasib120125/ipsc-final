<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <title>Ispahani</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="description" content="School and College from Bangladesh.">
  <meta name="keywords" content="School, College, University, Education, Result, Class Room, Exam, Notice, Routine, Rouf College, Rifles College, Principal, Chairman, Teacher , Staff">
  <meta name="author" content="#">
  <meta http-equiv="refresh" content="600">
  <meta name="robots" content="all">
  <meta name="googlebot" content="all">
  <meta name="googlebot-news" content="all">
  <meta name="rating" content="safe for kids">
  <link rel="canonical" href="#">
  <link type="image/x-icon" rel="shortcut icon" href="#">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/animate.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap-dropdownhover.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap-theme.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/full-slider.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/full-slider.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/mystyle.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/jBox.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/youtube-video-gallery.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/youtube-video-gallery.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <style type="text/css">
    
    .nav-tabs>li{
      float: none !important;
      display: inline-block;
    }

    .nav-tabs{
          display: inline-flex;
    }
  </style>

</head>


<header>
    <div class="container-fluid header-center">
        <div class="container">
          <div class="row">
            <div class="col-sm-2" style="padding-bottom: 3px;padding-top: 2px;"><a class="header-logo" href="{{URL::to('/')}}"><img src="{{asset('frontend/images/logo.png')}}" class="img-responsive" style="height: 132px; padding-left: 33px;"></a></div>

            <div class="col-sm-7" style="padding-top: 14px;
    margin-left: -16px;"> 
                <a href="#"><span class="header-name">ISPAHANI PUBLIC SCHOOL & COLLEGE, COMILLA</span></a><br>
                <h4 style="color: #fff; padding-top: 10px;">EIIN : 105826 | College Code : 0000 | School Code : 0000 </h4>
                <h5 style="color: #fff;margin-bottom: 12px;padding-top: 10px; font-size: 20px;"><i class="fa fa-envelope" aria-hidden="true"> ipsccml@gmail.com </i></h5>
            </div>

            <div class="col-sm-3" style="padding-bottom: 2px;">
                <h2 style="text-align: right;color: #ef492f;padding-top: 12px;font-size: 40px;font-weight: bold;">IPSC</h2>
                <button type="button" class="btn btn-primary btn-md" style="float: right;width: 95px;"><i class="fa fa-sign-in" aria-hidden="true" style="font-size: 17px;"> Login</i></button>
            </div>
          </div>
        </div>
    </div>

    <div class="container-fluid" style="background-color: #006b96 !important;">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <nav class="navbar" style="background-color: #006b96 !important;">
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="background-color: #ddd">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar" style="background: #013664 !important;"></span>
                      <span class="icon-bar" style="background: #013664 !important;"></span>
                      <span class="icon-bar" style="background: #013664 !important;"></span>
                  </button>
              </div>

              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                      <li><a  href="{{URL::to('/')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                      <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">About<b class="caret"></b></a>
                          <ul class="dropdown-menu">
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-globe" aria-hidden="true"> </i> &nbsp; IPSC at a Glance</a></li>
                              <li><a href="{{URL::to('/details')}}"> <i class="fa fa-book" aria-hidden="true"> </i> &nbsp; Why Study at IPSC</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-history" aria-hidden="true"> </i> &nbsp; History</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-university" aria-hidden="true"> </i> &nbsp; Infrastructure</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-shield" aria-hidden="true"> </i> &nbsp; Achievement</a></li>
                              <li><a href="{{URL::to('/news-events')}}"><i class="fa fa-newspaper-o" aria-hidden="true"> </i> &nbsp; News Events</a></li>
                          </ul>
                      </li>
                      <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administration <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                              <li><a href="{{URL::to('/governing-body')}}"><i class="fa fa-users" aria-hidden="true"></i> &nbsp; Governing Body</a></li>
                              <li><a href="{{URL::to('/messages')}}"><i class="fa fa-sticky-note-o" aria-hidden="true"></i> &nbsp; Message Chairman</a></li>
                              <li><a href="{{URL::to('/messages
                              ')}}"><i class="fa fa-sticky-note-o" aria-hidden="true"></i> &nbsp; Message Principal </a></li>
                              <li><a href="{{URL::to('/teacher-staff')}}"><i class="fa fa-user" aria-hidden="true"></i> &nbsp; Teacher</a></li>
                              <li><a href="{{URL::to('/teacher-staff')}}"><i class="fa fa-user" aria-hidden="true"></i> &nbsp; Staff</a></li>
                          </ul>
                      </li>
                      <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Academic <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                              <li><a href="{{URL::to('/academic')}}"><i class="fa fa-calendar" aria-hidden="true"></i> &nbsp;Academic Calendar</a></li>
                              <li><a href="{{URL::to('/academic')}}"><i class="fa fa-calendar-o" aria-hidden="true"></i> &nbsp;Holiday Calendar</a></li>
                              <li><a href="{{URL::to('/academic')}}"><i class="fa fa-file-text-o" aria-hidden="true"></i> &nbsp;Class Routine</a></li>
                              <li><a href="{{URL::to('/academic')}}"><i class="fa fa-file-o" aria-hidden="true"></i> &nbsp;Syllabus</a></li>
                              <li><a href="{{URL::to('/academic')}}"><i class="fa fa-file-text-o" aria-hidden="true"></i> &nbsp;Exam Routine</a></li>
                              <li><a href="{{URL::to('/academic')}}"><i class="fa fa-file-text-o" aria-hidden="true"></i> &nbsp;Public Exam Result</a></li>
                              <li><a href="{{URL::to('/academic')}}"><i class="fa fa-file-text-o" aria-hidden="true"></i> &nbsp;Internal Exam Result</a></li>
                              <li><a href="{{URL::to('/notice')}}"><i class="fa fa-bell-o" aria-hidden="true"></i> &nbsp;Notice</a></li>
                          </ul>
                      </li>
                      <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admission <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                              <li><a href="{{URL::to('/admission')}}"><i class="fa fa-th-large" aria-hidden="true"></i> &nbsp;Admission Circular</a></li>
                              <li><a href="{{URL::to('/admission')}}"><i class="fa fa-book" aria-hidden="true"></i> &nbsp;Prospectus</a></li>
                              <li><a href="{{URL::to('/admission')}}"><i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp;Admission Result</a></li>
                              <li><a href="{{URL::to('/admission')}}"><i class="fa fa-list-alt" aria-hidden="true"></i> &nbsp;Waiting List</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-qrcode" aria-hidden="true"></i> &nbsp;Courses/Programs</a></li>
                          </ul>
                      </li>

                      <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Facilities <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-building-o" aria-hidden="true"></i> &nbsp;Student Hostel</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-bus" aria-hidden="true"></i> &nbsp;Transport</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-building" aria-hidden="true"></i> &nbsp;Staff Quarter </a></li>
                          </ul>
                      </li>
                      <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Co-Curricular <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-spotify" aria-hidden="true"></i> &nbsp;Sports</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-plane" aria-hidden="true"></i> &nbsp;Tours</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-flag-o" aria-hidden="true"></i> &nbsp;Physical Activities</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-star-half-o" aria-hidden="true"></i> &nbsp;International Achievements</a></li>
                          </ul>
                      </li>
                      <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Club <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i> &nbsp;Debating Club</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i> &nbsp;Readers & Writers Club</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i> &nbsp;Extempore Speech Club</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i> &nbsp;Presentation Club</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i> &nbsp;Recitation Club</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i> &nbsp;Music Club</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i> &nbsp;Dance Club</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i> &nbsp;Science Club</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i> &nbsp;ICT Club</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i> &nbsp;UNESCO Club</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i> &nbsp;Art Club</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i> &nbsp;General Knowledge Club</a></li>
                              <li><a href="{{URL::to('/details')}}"><i class="fa fa-pied-piper-alt" aria-hidden="true"></i> &nbsp;Language Club</a></li>
                          </ul>
                      </li>
                      <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gallery <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                              <li><a href="{{URL::to('/photo-gallery')}}"><i class="fa fa-camera" aria-hidden="true"></i>  Photo Gallery</a></li>
                              <li><a href="{{URL::to('/video-gallery')}}"><i class="fa fa-file-video-o" aria-hidden="true"></i>  Video Gallery</a></li>
                          </ul>
                      </li>
                    </ul> 
              </div>
          </nav>
          </div>
        </div>
      </div>
    </div>
</header>

<script src="{{asset('frontend/js/myjs.js')}}"></script>

<script>
        $('#myCarousel').carousel({
            interval:   4000
        });
</script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/TweenMax.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/plugins/ScrollToPlugin.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/plugins/EaselPlugin.min.js"></script>
  <script src="{{asset('frontend/js/animate-scroll.js')}}"></script>
  <script type="text/javascript" src="{{asset('frontend/js/animate-scroll.js')}}"></script>
  <script type="text/javascript" src="{{asset('frontend/js/bootstrap-dropdownhover.min.js')}}"></script>

  <script type="text/javascript" src="{{asset('frontend/js/css3-animate-it.js')}}"></script>
  <script type="text/javascript" src="{{asset('frontend/js/jBox-min.js')}}"></script>

  <script type="text/javascript" src="{{asset('frontend/js/jquery.youtubevideogallery.js')}}"></script>

  <script type="text/javascript" src="{{asset('frontend/js/myjs.js')}}"></script>

  <script type="text/javascript" src="{{asset('frontend/js/npm.js')}}"></script>

 
<script>
  $(document).foundation();
  $(document).animateScroll();

  var doc = document.documentElement;
  doc.setAttribute('data-useragent', navigator.userAgent);
</script>
  <!-- Bootstrap Dropdown Hover JS -->