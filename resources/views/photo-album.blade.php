@include('include.header')

<div class="container-fluid body-bg">
    <div class="container margin-top10">
        <div class="row">
            <div class="col-sm-8" style="margin-bottom: 10px">
                <div class="row top-head">
                    <h1>Album Group name - </h1>
                </div>

                <div class="row">
                    <div class="col-sm-4" style="margin: 0;padding: 0;">
                        <a href="photo_details.php">
                            <img src="{{asset('frontend/images/school-image2.jpg')}}" class="img-responsive img-thumbnail img-height" alt="2018" title="2018">
                            <span><strong class="margin-top10">Gallery : 2018</strong><br>
                               3 Albums | Views : 184 views
                            </span>
                         </a>
                    </div>

                    <div class="col-sm-4" style="margin: 0;padding: 0;">
                        <a href="photo_details.php">
                            <img src="{{asset('frontend/images/school-image2.jpg')}}" class="img-responsive img-thumbnail img-height" alt="2018" title="2018">
                            <span><strong class="margin-top10">Gallery : 2018</strong><br>
                               3 Albums | Views : 184 views
                            </span>
                         </a>
                    </div>

                    <div class="col-sm-4" style="margin: 0;padding: 0;">
                        <a href="photo_details.php">
                            <img src="{{asset('frontend/images/school-image2.jpg')}}" class="img-responsive img-thumbnail img-height" alt="2018" title="2018">
                            <span><strong class="margin-top10">Gallery : 2018</strong><br>
                               3 Albums | Views : 184 views
                            </span>
                         </a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4" style="margin: 0;padding: 0;">
                        <a href="photo_details.php">
                            <img src="{{asset('frontend/images/school-image2.jpg')}}" class="img-responsive img-thumbnail img-height" alt="2018" title="2018">
                            <span><strong class="margin-top10">Gallery : 2018</strong><br>
                               3 Albums | Views : 184 views
                            </span>
                         </a>
                    </div>

                    <div class="col-sm-4" style="margin: 0;padding: 0;">
                        <a href="photo_details.php">
                            <img src="{{asset('frontend/images/school-image2.jpg')}}" class="img-responsive img-thumbnail img-height" alt="2018" title="2018">
                            <span><strong class="margin-top10">Gallery : 2018</strong><br>
                               3 Albums | Views : 184 views
                            </span>
                         </a>
                    </div>

                    <div class="col-sm-4" style="margin: 0;padding: 0;">
                        <a href="photo_details.php">
                            <img src="{{asset('frontend/images/school-image2.jpg')}}" class="img-responsive img-thumbnail img-height" alt="2018" title="2018">
                            <span><strong class="margin-top10">Gallery : 2018</strong><br>
                               3 Albums | Views : 184 views
                            </span>
                         </a>
                    </div>
                </div>

            </div>

            <aside>
                <div class="col-sm-4 float-right">
                    <div class="col-xs-12 col-sm-12">
                        <div class="row top-head-right">
                            <a href="#"><i class="fa fa-home" aria-hidden="true"></i></a> / Gallery / Album Folder
                        </div>

                        <div class="row right-video">
                            <i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video
                        </div>
                        <div class="row margin-bottom20">
                            <iframe width="360" height="195" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe>
                        </div>

                        <div class="row related-topics box-shado margin-bottom12p">
                            <a href="video-gallery.php"><img src="{{asset('frontend/images/videogallery-ico.png')}}" class="img-responsive"></a>
                        </div>

                    </div>
                </div>
            </aside>
        </div>
    </div>
</div>

@include('include.footer')