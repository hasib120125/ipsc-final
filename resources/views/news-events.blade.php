@include('include.header')


<div class="container-fluid body-bg">
    <div class="container margin-top10">
        <div class="row">
            <div class="col-sm-8">
                <div class="row top-head">
                    <h1>Ispahani Public School & College News & Events</h1>
                </div>
                <div class="row" style="margin-bottom: 2%">
                    <article>
                        <div class="col-xs-3 col-sm-3 padding-left0"><img src="https://www.abdurroufcollege.ac.bd/media/imgAll/sm/23-05-2017-sm-1495548138.jpg" class="img-responsive margin-bottom2P" alt="" title=""></div>
                        <div class="col-xs-9 col-sm-9 padding-left0">
                            <span>May 22, 2017 | Views : 482 views</span>
                            <h3>ভারতের মেঘালয়ে বিএসএফ কর্তৃক পরিচালিত প্রতিষ্ঠানে গান পরিবেশন করছেন প্রতিষ্ঠানের শিক্ষক এবং শিক্ষার</h3>
                             <a href="{{URL::to('details')}}">Read more...</a>
                        </div>
                    </article>
                </div>

                <div class="row" style="margin-bottom: 2%">
                    <article>
                        <div class="col-xs-3 col-sm-3 padding-left0"><img src="https://www.abdurroufcollege.ac.bd/media/imgAll/sm/23-05-2017-sm-1495548138.jpg" class="img-responsive margin-bottom2P" alt="" title=""></div>
                        <div class="col-xs-9 col-sm-9 padding-left0">
                            <span>May 22, 2017 | Views : 482 views</span>
                            <h3>ভারতের মেঘালয়ে বিএসএফ কর্তৃক পরিচালিত প্রতিষ্ঠানে গান পরিবেশন করছেন প্রতিষ্ঠানের শিক্ষক এবং শিক্ষার</h3>
                             <a href="{{URL::to('details')}}">Read more...</a>
                        </div>
                    </article>
                </div>

                <div class="row" style="margin-bottom: 2%">
                    <article>
                        <div class="col-xs-3 col-sm-3 padding-left0"><img src="https://www.abdurroufcollege.ac.bd/media/imgAll/sm/23-05-2017-sm-1495548138.jpg" class="img-responsive margin-bottom2P" alt="" title=""></div>
                        <div class="col-xs-9 col-sm-9 padding-left0">
                            <span>May 22, 2017 | Views : 482 views</span>
                            <h3>ভারতের মেঘালয়ে বিএসএফ কর্তৃক পরিচালিত প্রতিষ্ঠানে গান পরিবেশন করছেন প্রতিষ্ঠানের শিক্ষক এবং শিক্ষার</h3>
                             <a href="{{URL::to('details')}}">Read more...</a>
                        </div>
                    </article>
                </div>

                <div class="row">
                    <ul class="pagination">
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-xs-12 col-sm-12">
                    <div class="row top-head-right">
                        <a href="#"><i class="fa fa-home" aria-hidden="true"></i></a> / <a href="#">About / News & Events</a>
                    </div>

                    <div class="row right-video">
                        <i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video
                    </div>
                    <div class="row margin-bottom20">
                        <iframe width="360" height="195" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe>
                    </div>

                    <div class="row related-topics box-shado">
                        <i class="fa fa-windows" aria-hidden="true"></i> Related Topics
                    </div>
                    <div class="row margin-bottom20">
                        @include("aside/about-us-aside")
                    </div>
                </div>
            </div>
    </div>
</div>


@include('include.footer')