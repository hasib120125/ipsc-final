@include('include.header')

<div class="container" style="margin-top: 10px;margin-bottom: 10px;">
    <div class="row">
        <div class="col-sm-12">
            <div class="col-xs-12 col-sm-12 box mar-top10 mar-bottom10 padding10">
                <iframe width="1090" height="500" src="https://www.youtube.com/embed/IFBVaVa77aw" frameborder="0" allowfullscreen=""></iframe>
            </div>

            <div class="col-xs-12 col-sm-12 box mar-top10 mar-bottom10 padding10">
                <h2 class="head-h1" style="padding-left:30px; padding-bottom:10px;">More Videos of Ispahani Public School & College</h2>
                <div class="col-sm-12" style="padding-bottom:10px;">
                    <div class="col-sm-2 mar">
                        <a href="#">
                            <img src="http://img.youtube.com/vi/Wou2mlL6qS8/0.jpg" class="img-responsive"> Ispahani Public School & College                        
                        </a>
                    </div>
                
                    <div class="col-sm-2 mar">
                        <a href="#">
                            <img src="http://img.youtube.com/vi/sQoDZAsMP4Y/0.jpg" class="img-responsive">Governing Body Ispahani Public School & College                        
                        </a>
                    </div>
                    <div class="col-sm-2 mar">
                        <a href="#">
                            <img src="http://img.youtube.com/vi/OqqmjsHOErg/0.jpg" class="img-responsive">BHEC in BTV News 2015                       
                        </a>
                    </div>
                    <div class="col-sm-2 mar">
                        <a href="#">
                            <img src="http://img.youtube.com/vi/oQGfAJ9lKi8/0.jpg" class="img-responsive">Best College News Channel 24 2017                        
                        </a>
                    </div>
                </div>
            </div>       
        </div>
    </div>
</div>

@include('include.footer')