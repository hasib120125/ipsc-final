<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <title>Ispahani</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="description" content="School and College from Bangladesh.">
  <meta name="keywords" content="School, College, University, Education, Result, Class Room, Exam, Notice, Routine, Rouf College, Rifles College, Principal, Chairman, Teacher , Staff">
  <meta name="author" content="#">
  <meta http-equiv="refresh" content="600">
  <meta name="robots" content="all">
  <meta name="googlebot" content="all">
  <meta name="googlebot-news" content="all">
  <meta name="rating" content="safe for kids">
  <link rel="canonical" href="#">
  <link type="image/x-icon" rel="shortcut icon" href="#">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/animate.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap-dropdownhover.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap-theme.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/full-slider.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/full-slider.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/mystyle.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/jBox.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/youtube-video-gallery.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/youtube-video-gallery.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <style type="text/css">
    
    .nav-tabs>li{
      float: none !important;
      display: inline-block;
    }

    .nav-tabs{
          display: inline-flex;
    }
  </style>

  </head>
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
          fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
  </script>
  <body>
    	@include("include/header")
    <header id="myCarousel" class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>

        <div class="carousel-inner">
            @include("include/home-slide")
        </div>

        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>

    
    <div class="container-fluid padding-left0 padding-right0">
      <div class="col-sm-6 slide-bottom-left" align="center" style="position: static !important;">
        <h2 class="h2" style="margin: 0px !important">Why Study at <span>IPSC</span></h2>
        <p class="txt-color">The institution offers a good number of co-curricular activities such as. B CC army (male and female), BNCC navy (male) B CC Air (Male and Female), Army (Junior) Holde Pakhi, Rover Scouts (Air, Boys, Girls) Cub Scout, Air Scout, Navy Scout, Band group.English and Debate, Music, Dance, Display, Football, Volleyball, Basketball. </p>
          <a class="btn btn-primary margin-top7" href="why_study_en.php">Read More</a>
      </div>

      <div class="col-sm-6 slide-bottom-right" style="position: static !important;float: right;color: #000;background: #003B6E;">
        <div class="col-sm-2 img-dedication"><img src="{{asset('frontend/images/ipsc.png')}}" class="img-responsive"></div>
        <div class="col-sm-10">
          <h2 class="h2">Dedication</h2>
          <p class="txt-color">১৯৬৫ সালের জুন মাসে ময়নামতি পাবলিক স্কুল হিসাবে নামকরণ করা হয়।[৩] ১৯৬৬ সালে ২৮শে সেপ্টেম্বর দানবীর মির্জা আহমেদ ইস্পাহানীর আর্থিক সহায়তায় গ্রহণের পর প্রতিষ্ঠানটির নাম ইস্পাহানী পাবলিক স্কুল নাম করন করা হয়। পরে ১৯৭৫ সালের ২৮শে অক্টোবর প্রতিষ্ঠানটিতে কলেজ শাখা করার পর প্রতিষ্ঠানটির সর্বশেষ নাম করন করা হয় ইস্পাহানী পাবলিক স্কুল এন্ড কলেজ।</p>
          <a class="btn btn-primary margin-top7" href="details.php">Read More</a>
        </div>
      </div>
    </div>


    <div class="container-fluid about-us">
      <div class="container" align="center">
        <div class="row margin-bottom10" data-animate-scroll='{"x": "100","y":"100","rotation":"-25","alpha": "0","duration": "1.5","scaleX": "0","scaleY": "0"}'>
            <span class="about-text">About us</span>
        </div>
        @include("include/about_us_en")
        <a href="about_us_bn.php" class="btn btn-primary margin-top5P" data-animate-scroll='{"alpha": "0","y":"50","duration": "1.75","scaleX": "0","scaleY": "0","ease": "Elastic.easeIn"}'>Read in Bangla</a>
      </div>
    </div>

    
    <div class="container-fluid msg-bg">
        <div class="container" align="center">
            <span class="msg-text">Message from Chief Patrom</span>
            <div class="row">
                <div class="col-sm-4"></div>
                	@include('include/home-chairman')
                <div class="col-sm-4"></div>
            </div>
            
            <div class="row">
                <div class="col-sm-1"></div>
                	@include('include/home-vice-chairman')
                	@include('include/home-principal')
                <div class="col-sm-2"></div>
            </div><br><br><br>
        </div>
    </div>


    <section class="home-life border-diag-bottom">
      <div class="container-fluid">
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
              <div class="home-life-side-img" style="background-image: url({{asset('frontend/images/home_right.jpg')}})"></div>
            </div>

            <div class="col-sm-4" style="padding-top: 100px;">
              <div class="home-life" align="center">
                <span class="about-text">At a Glance</span>
                  	@include("include/at_a_glance")
                  <a href="about_details.php" class="btn btn-primary margin-top5P">Read More</a>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="home-life-side-img" style="background-image: url({{asset('frontend/images/home_left.jpg')}})"></div>
            </div>
          </div>
        </div>
      </div>
    </section>



      <section class="result-bg">
        <div class="container-fluid" style="padding-top: 28px;padding-bottom: 100px;">
           <div class="container">
              <h4 class="result-head" style="text-align: center">Institute Results</h4>
            <div class="row">
              <div class="col-sm-12">
                <div class="container" align="center">
                           <ul class="nav nav-tabs margin-top10">
                               <li><a data-toggle="tab" href="#menu1" style="background-color: #fff; color: #000 !important">PSC</a></li>
                               <li><a data-toggle="tab" href="#menu2" style="background-color: #fff; color: #000 !important">JSC</a></li>
                               <li><a data-toggle="tab" href="#menu3" style="background-color: #fff; color: #000 !important">SSC</a></li>
                               <li><a data-toggle="tab" href="#menu4" style="background-color: #fff; color: #000 !important">HSC</a></li>
                           </ul>

                       <div class="tab-content">

                           <div id="menu1" class="tab-pane fade">
                               <h3>PSC Result</h3>
                               <h4>IPSC</h4>
                               <br>
                               <a href="academic.php" class="btn btn-info" target="_blank">Read More</a>
                           </div>

                           <div id="menu2" class="tab-pane fade">
                               <h3>JSC Result</h3>
                               <h4>IPSC</h4>
                               <br>
                               <a href="academic.php" class="btn btn-info" target="_blank">Read More</a>
                           </div>

                           <div id="menu3" class="tab-pane fade">
                               <h3>SSC Result</h3>
                              
                               <h4>IPSC</h4>
                               <br>
                               <a href="academic.php" class="btn btn-info" target="_blank">Read More</a>
                           </div>

                           <div id="menu4" class="tab-pane fade">
                               <h3>HSC Result</h3>
                              
                               <h4>IPSC</h4>
                               <br>
                               <a href="academic.php" class="btn btn-info" target="_blank">Read More</a>
                           </div>
                       </div>
                   </div>
              </div>
              </div>
           </div>
        </div>
      </section>

    
      <section>
        <div class="container-fluid">
          <div class="row">
          <div class="col-sm-6">
            <div class="row video-bg" align="center">
              <div class="col-sm-12" style="padding-top: 17px;">
                <h2 class="video-title">Our Featured Video</h2>
                <p class="video-text">Conservators in Collections Care repair a book with a broken cover to prevent further damage. Protecting Harvard’s special collections of rare books, manuscripts, prints, drawings, maps, photographs, and other treasures is the mission of Harvard Library Preservation Services.</p>
              </div>

              <div class="col-xs-12 col-sm-12" style="padding-top: 6px;">
                <iframe class="iframe-wh" src="https://www.youtube.com/embed/iu-naJgj3I8" frameborder="0" allowfullscreen></iframe>
              </div>

              <a href="video-gallery.php" class="btn btn-primary margin-top5P margin-bottom2P">WATCH MORE VIDEO</a>
            </div>

            <div class="row fb-bg">
              <div class="col-xs-12 col-sm-12" align="center">
                  <h2 class="fb-title">Facebook Official Fan Page</h2>
                  <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fipsc.ispahanian%2F&tabs=timeline&width=350&height=181&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId" width="350" height="181" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
              </div>
            </div>
          </div>



          <div class="col-sm-6">
            <div class="row notice-bg">
              <div class="col-sm-12">
                  <div class="row notice-margin">
                      <div class="col-sm-12">
                      <span class="glance-text">Latest Notice</span>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-12">
                          	@include('include/home-notice')
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
        <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <div class="row achivement-bg">
              <div class="col-sm-12 margin-top5P">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-9"><span class="achivement-head">Achivements</span></div>
              </div>

              <div class="col-sm-12 margin-top5P">
                  <div class="col-sm-9">
                      	@include('include/home-achievements')
                  </div>
              </div>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="row events-bg">
              <div class="col-sm-12 margin-top5P">
                  <div class="col-sm-9"><span class="events-head">Events</span></div>
                  <div class="col-sm-3"></div>
              </div>

              <div class="col-sm-12 margin-top5P margin-left5P">
                  <div class="col-sm-9">
                      	@include('include/home-news-events')
                  </div>

                  <div class="col-sm-3"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
          <div class="container-fluid contact-bg">
              <div class="row">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-sm-12 contact-bg-img contact">
                        <h2>Contact Us </h2><hr>
                        <h3 style="padding-bottom: 8px;">Ispahani Public School & College </h3>
                        <h3 style="padding-bottom: 8px;">Comilla Cantonment 3501</h3>
                        <h3 style="padding-bottom: 8px;">Phone : +880 817-6766</h3>
                        <h3 style="padding-bottom: 8px;">Email : ipsccml@gmail.com</h3>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12 google-map">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3659.5479476790024!2d91.11728921465436!3d23.47676518472499!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x37547bf45642b5af%3A0x9760ec0d98f2bcdc!2z4KaH4Ka44KeN4Kaq4Ka-4Ka54Ka-4Kao4KeAIOCmquCmvuCmrOCmsuCmv-CmlSDgprjgp43gppXgp4HgprIg4KaP4Kao4KeN4KahIOCmleCmsuCnh-CmnA!5e0!3m2!1sbn!2sbd!4v1522665596358" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
              </div>
        </div>
    </section>

  	@include("include/footer")

    <script src="{{asset('frontend/js/myjs.js')}}"></script>
    <script>
        $('#myCarousel').carousel({
            interval:   4000
        });
    </script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/TweenMax.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/plugins/ScrollToPlugin.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/plugins/EaselPlugin.min.js"></script>
  <script src="{{asset('frontend/js/animate-scroll.js')}}"></script>
  <script type="text/javascript" src="{{asset('frontend/js/animate-scroll.js')}}"></script>
  <script type="text/javascript" src="{{asset('frontend/js/bootstrap-dropdownhover.min.js')}}"></script>

  <script type="text/javascript" src="{{asset('frontend/js/css3-animate-it.js')}}"></script>
  <script type="text/javascript" src="{{asset('frontend/js/jBox-min.js')}}"></script>

  <script type="text/javascript" src="{{asset('frontend/js/jquery.youtubevideogallery.js')}}"></script>

  <script type="text/javascript" src="{{asset('frontend/js/myjs.js')}}"></script>

  <script type="text/javascript" src="{{asset('frontend/js/npm.js')}}"></script>

 
  <script>
    $(document).foundation();
    $(document).animateScroll();

    var doc = document.documentElement;
    doc.setAttribute('data-useragent', navigator.userAgent);
  </script>
  <!-- Bootstrap Dropdown Hover JS -->

  </body>
</html>