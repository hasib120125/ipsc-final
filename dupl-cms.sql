-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 18, 2018 at 12:10 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dupl-cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_album_category`
--

CREATE TABLE `cms_album_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `alct_name` varchar(69) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alct_bangla_name` varchar(69) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alct_image_path` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alct_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `alct_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alct_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_album_category`
--

INSERT INTO `cms_album_category` (`id`, `alct_name`, `alct_bangla_name`, `alct_image_path`, `alct_is_active`, `alct_user_ip`, `alct_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Album Category English Name -1', 'Album Category Bangla Name -1', '2018/1522735098.jpg', 'Y', '::1', '1', 'EOijFl1R2CQLovycZnAKX1tvELVzAFPJfwVHWHrF', '2018-04-17 04:17:19', '2018-04-17 04:17:19'),
(2, 'Album Category English Name -2', 'Album Category Bangla Name -1', '2018/1522735195.jpg', 'Y', '::1', '1', 'BwBPMyyJe5gzrY3aOMfRoDwaaSFqjZwWDEcYirLj', '2018-04-03 05:59:55', '2018-04-03 05:59:55'),
(3, 'Album Category English Name -3', 'Album Category Bangla Name -1', '2018/1522735224.jpg', 'Y', '::1', '1', 'BwBPMyyJe5gzrY3aOMfRoDwaaSFqjZwWDEcYirLj', '2018-04-03 06:00:24', '2018-04-03 06:00:24'),
(4, 'Album Category English Name -4', 'Album Category Bangla Name -1', '2018/1522735308.jpg', 'Y', '::1', '1', 'BwBPMyyJe5gzrY3aOMfRoDwaaSFqjZwWDEcYirLj', '2018-04-03 06:01:48', '2018-04-03 06:01:48'),
(5, 'Album Category English Name -5', NULL, '2018/1522736025.jpg', 'Y', '::1', '1', 'BwBPMyyJe5gzrY3aOMfRoDwaaSFqjZwWDEcYirLj', '2018-04-03 06:13:45', '2018-04-03 06:13:45'),
(6, 'Album Category English Name -6', NULL, '2018/1522736029.jpg', 'Y', '::1', '1', 'BwBPMyyJe5gzrY3aOMfRoDwaaSFqjZwWDEcYirLj', '2018-04-03 06:13:49', '2018-04-03 06:13:49'),
(7, 'Album Category English Name -7', NULL, '2018/1522736031.jpg', 'Y', '::1', '1', 'BwBPMyyJe5gzrY3aOMfRoDwaaSFqjZwWDEcYirLj', '2018-04-03 06:13:51', '2018-04-03 06:13:51'),
(8, 'Album Category English Name -8', NULL, '2018/1522736033.jpg', 'Y', '::1', '1', 'BwBPMyyJe5gzrY3aOMfRoDwaaSFqjZwWDEcYirLj', '2018-04-03 06:13:53', '2018-04-03 06:13:53'),
(9, 'Album Category English Name -9', 'xxxxxxxxxxxxxx', '2018/1522736035.jpg', 'Y', '::1', '1', 'BwBPMyyJe5gzrY3aOMfRoDwaaSFqjZwWDEcYirLj', '2018-04-03 06:29:04', '2018-04-03 06:29:04'),
(10, 'Album Category English Name -10', 'zxxxxx', '2018/1522736858.jpg', 'Y', '::1', '1', 'BwBPMyyJe5gzrY3aOMfRoDwaaSFqjZwWDEcYirLj', '2018-04-03 06:27:38', '2018-04-03 06:27:38'),
(11, 'Album Category English Name -11', NULL, '2018/1522736042.jpg', 'Y', '::1', '1', 'BwBPMyyJe5gzrY3aOMfRoDwaaSFqjZwWDEcYirLj', '2018-04-03 06:14:02', '2018-04-03 06:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `cms_album_group`
--

CREATE TABLE `cms_album_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `algp_album_cat_id` int(11) NOT NULL,
  `algp_name` varchar(69) COLLATE utf8mb4_unicode_ci NOT NULL,
  `algp_bangla_name` varchar(69) COLLATE utf8mb4_unicode_ci NOT NULL,
  `algp_image_path` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `algp_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `algp_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `algp_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_album_group`
--

INSERT INTO `cms_album_group` (`id`, `algp_album_cat_id`, `algp_name`, `algp_bangla_name`, `algp_image_path`, `algp_is_active`, `algp_user_ip`, `algp_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Album Group Name - 1', 'Album Group Name  Bangla - 1', '2018/1522743891.jpg', 'Y', '::1', '1', 'BwBPMyyJe5gzrY3aOMfRoDwaaSFqjZwWDEcYirLj', '2018-04-03 08:24:51', '2018-04-03 08:24:51'),
(2, 2, 'Album Group Name - 2', 'Album Group Name  Bangla - 2', '2018/1522744080.jpg', 'Y', '::1', '1', 'BwBPMyyJe5gzrY3aOMfRoDwaaSFqjZwWDEcYirLj', '2018-04-03 09:03:53', '2018-04-03 09:03:53'),
(3, 3, 'Album Group Name - 3', 'Album Group Name  Bangla - 3', '2018/1522746259.jpg', 'Y', '::1', '1', 'BwBPMyyJe5gzrY3aOMfRoDwaaSFqjZwWDEcYirLj', '2018-04-03 09:04:19', '2018-04-03 09:04:19'),
(4, 1, 'Album Group Name - 4', 'Album Group Name  Bangla - 4', '2018/1522744712.jpg', 'Y', '::1', '1', 'xsekKdoJ0wzYgH6hBKO6AjI4kkqwvjvSy4kihCMm', '2018-04-04 05:58:07', '2018-04-04 05:58:07');

-- --------------------------------------------------------

--
-- Table structure for table `cms_album_image`
--

CREATE TABLE `cms_album_image` (
  `id` int(10) UNSIGNED NOT NULL,
  `alim_album_cat_id` tinyint(4) NOT NULL,
  `alim_album_group_id` tinyint(4) NOT NULL,
  `alim_english_title` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alim_bangla_title` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alim_image_path` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alim_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `alim_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alim_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_album_image`
--

INSERT INTO `cms_album_image` (`id`, `alim_album_cat_id`, `alim_album_group_id`, `alim_english_title`, `alim_bangla_title`, `alim_image_path`, `alim_is_active`, `alim_user_ip`, `alim_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 1, 1, NULL, NULL, '2018/1522921555.jpg', 'Y', '::1', '1', 'l9q6yEwmLb4k03yeOdU4IEMH0wwzjHXxgmQINXok', '2018-04-05 09:45:55', '2018-04-05 09:45:55'),
(4, 2, 2, 'xx', 'xxx', '2018/1522921633.jpg', 'Y', '::1', '1', 'l9q6yEwmLb4k03yeOdU4IEMH0wwzjHXxgmQINXok', '2018-04-05 09:47:13', '2018-04-05 09:47:13');

-- --------------------------------------------------------

--
-- Table structure for table `cms_blood_group`
--

CREATE TABLE `cms_blood_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `bdgp_name` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bdgp_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_blood_group`
--

INSERT INTO `cms_blood_group` (`id`, `bdgp_name`, `bdgp_is_active`) VALUES
(1, 'O+', 'Y'),
(2, 'A+', 'Y'),
(3, 'B+', 'Y'),
(4, 'AB+', 'Y'),
(5, 'O-', 'Y'),
(6, 'A-', 'Y'),
(7, 'B-', 'Y'),
(8, 'AB-', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `cms_board`
--

CREATE TABLE `cms_board` (
  `id` int(10) UNSIGNED NOT NULL,
  `boar_name` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `boar_bangla_name` varchar(49) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boar_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `boar_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `boar_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_board`
--

INSERT INTO `cms_board` (`id`, `boar_name`, `boar_bangla_name`, `boar_is_active`, `boar_user_ip`, `boar_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dhaka', NULL, 'Y', '::1', '1', 'Tp5RNTZL4LubbCqCJulT74eZ6ax3dqqdNHmw5Cpw', '2018-03-15 04:12:21', '2018-03-15 04:12:21');

-- --------------------------------------------------------

--
-- Table structure for table `cms_contents_category`
--

CREATE TABLE `cms_contents_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `coca_name` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coca_bangla_name` varchar(49) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coca_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `coca_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coca_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_contents_category`
--

INSERT INTO `cms_contents_category` (`id`, `coca_name`, `coca_bangla_name`, `coca_is_active`, `coca_user_ip`, `coca_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Departmental  Notice', 'অামাদের কথা', 'N', '::1', '1', 'SuSDZsFbdGB0sm1TSndx8DnrqDrVWwmAJxmuIMxW', '2018-03-19 10:43:37', '2018-03-28 06:31:54'),
(2, 'About Us', NULL, 'Y', '::1', '1', 'SuSDZsFbdGB0sm1TSndx8DnrqDrVWwmAJxmuIMxW', '2018-03-19 10:43:46', '2018-03-28 04:52:53'),
(3, 'Magazine', NULL, 'Y', '::1', '1', 'XtHW2HM3orZczk9lnFfCQpRjkWryZitba4VRtLec', '2018-03-20 11:01:46', '2018-03-20 11:01:46'),
(4, 'Office', NULL, 'Y', '::1', '1', 'SuSDZsFbdGB0sm1TSndx8DnrqDrVWwmAJxmuIMxW', '2018-03-28 05:47:48', '2018-03-28 05:47:48');

-- --------------------------------------------------------

--
-- Table structure for table `cms_contents_master`
--

CREATE TABLE `cms_contents_master` (
  `id` int(10) UNSIGNED NOT NULL,
  `coma_cat_id` int(7) NOT NULL,
  `coma_eng_title` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coma_bng_title` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coma_eng_brief` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coma_bng_brief` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coma_eng_details` varchar(199) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coma_bng_details` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coma_img_path` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coma_video_id` varchar(69) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coma_file_path` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coma_total_hits` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coma_is_download` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `coma_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `coma_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coma_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_contents_master`
--

INSERT INTO `cms_contents_master` (`id`, `coma_cat_id`, `coma_eng_title`, `coma_bng_title`, `coma_eng_brief`, `coma_bng_brief`, `coma_eng_details`, `coma_bng_details`, `coma_img_path`, `coma_video_id`, `coma_file_path`, `coma_total_hits`, `coma_is_download`, `coma_is_active`, `coma_user_ip`, `coma_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 2, 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>xxxxxxxxxxxxxxxxxxxxx</p>', '<p>xxxxxxxxxxxxxxxxxxxxxxxx</p>', '<p>xxxxxxxxxxxxxxxxxxxxxxx</p>', '<p>xxxxxxxxxxxxxxxxxxxxxxxxx</p>', '2018/1521702333.jpg', 'xxxxx', NULL, NULL, 'N', 'Y', '::1', '1', 'crAlZhdXAUj8B67tnHnXjWCCwGsy9BYjacxSJUdb', '2018-03-22 07:05:33', '2018-04-17 03:27:29'),
(2, 2, 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>xxxxxxxx</p>', '<p>xxxxxxxxxxxxxxx</p>', '<p>xxxxxxxxxxxxx</p>', '<p>xxxxxxxxxxxxxxx</p>', '2018/1521706080.jpg', NULL, NULL, NULL, 'N', 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 08:08:00', '2018-03-22 08:08:00'),
(3, 2, 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>xxxx</p>', '<p>xxxxx</p>', '<p>xxxx</p>', '<p>xxxx</p>', '2018/1521706123.jpg', NULL, NULL, NULL, 'N', 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 08:08:43', '2018-03-22 08:08:43'),
(4, 2, 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>dddd</p>', '<p>dfdfd</p>', '<p>fsdfdf</p>', '<p>sdfssf</p>', '2018/1521706153.jpg', NULL, NULL, NULL, 'N', 'N', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 08:09:13', '2018-03-22 08:22:16'),
(5, 2, 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>xxxxxxx</p>', '<p>xxxxxxxxxxxx</p>', '<p>xxxxxxx</p>', '<p>xxxxxxxxxx</p>', '2018/1521706224.jpg', NULL, NULL, NULL, 'N', 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 08:10:24', '2018-03-22 08:10:24'),
(6, 2, 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>xxxx</p>', '<p>xxxx</p>', '<p>xxxx</p>', '<p>xxxx</p>', '2018/1521706259.jpg', NULL, NULL, NULL, 'N', 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 08:10:59', '2018-03-22 08:10:59'),
(7, 2, 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>xxxx</p>', '<p>xxxx</p>', '<p>xxxx</p>', '<p>xxxx</p>', '2018/1521706287.jpg', NULL, NULL, NULL, 'N', 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 08:11:27', '2018-03-22 08:11:27'),
(8, 2, 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>xxx</p>', '<p>xxx</p>', '<p>xxx</p>', '<p>xxx</p>', '2018/1521706307.jpg', NULL, NULL, NULL, 'N', 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 08:11:47', '2018-03-22 08:11:47'),
(9, 2, 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>xxxx</p>', '<p>xxxx</p>', '<p>xxxx</p>', '<p>xxxx</p>', '2018/1521706330.jpg', NULL, NULL, NULL, 'N', 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 08:12:10', '2018-03-22 08:12:10'),
(10, 2, 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>xxx</p>', '<p>xxx</p>', '<p>xxx</p>', '<p>xxx</p>', '2018/1521706590.jpg', NULL, NULL, NULL, 'N', 'Y', '::1', '1', 'SuSDZsFbdGB0sm1TSndx8DnrqDrVWwmAJxmuIMxW', '2018-03-22 08:16:30', '2018-03-28 04:45:40'),
(11, 2, 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>xxx</p>', '<p>xxx</p>', '<p>xxx</p>', '<p>xxx</p>', '2018/1521706614.jpg', NULL, NULL, NULL, 'N', 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 08:16:54', '2018-03-22 08:16:54'),
(12, 2, 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>xxx</p>', '<p>xxx</p>', '<p>xx</p>', '<p>xx</p>', '2018/1521706630.jpg', NULL, NULL, NULL, 'N', 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 08:17:10', '2018-03-22 08:17:10');

-- --------------------------------------------------------

--
-- Table structure for table `cms_designation`
--

CREATE TABLE `cms_designation` (
  `id` int(10) UNSIGNED NOT NULL,
  `desi_name` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desi_bangla_name` varchar(49) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desi_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `desi_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desi_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_designation`
--

INSERT INTO `cms_designation` (`id`, `desi_name`, `desi_bangla_name`, `desi_is_active`, `desi_user_ip`, `desi_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Teacher', NULL, 'Y', '::1', '1', 'Tp5RNTZL4LubbCqCJulT74eZ6ax3dqqdNHmw5Cpw', '2018-03-15 04:12:32', '2018-03-15 04:12:32');

-- --------------------------------------------------------

--
-- Table structure for table `cms_group`
--

CREATE TABLE `cms_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `grou_name` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grou_bangla_name` varchar(49) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grou_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `grou_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grou_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_group`
--

INSERT INTO `cms_group` (`id`, `grou_name`, `grou_bangla_name`, `grou_is_active`, `grou_user_ip`, `grou_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Science', NULL, 'Y', '::1', '1', 'KURsFQZYCtBX6phtyGJ3ltsk0lgrXl4FBh0pKCNB', '2018-03-15 04:11:00', '2018-04-17 03:35:46');

-- --------------------------------------------------------

--
-- Table structure for table `cms_messages`
--

CREATE TABLE `cms_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `mess_cat_id` varchar(19) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mess_eng_title` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mess_bng_title` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mess_eng_brief` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mess_bng_brief` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mess_eng_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mess_bng_details` text COLLATE utf8mb4_unicode_ci,
  `mess_img_path` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mess_video_id` varchar(69) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mess_file_path` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mess_total_hits` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mess_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `mess_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mess_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_messages`
--

INSERT INTO `cms_messages` (`id`, `mess_cat_id`, `mess_eng_title`, `mess_bng_title`, `mess_eng_brief`, `mess_bng_brief`, `mess_eng_details`, `mess_bng_details`, `mess_img_path`, `mess_video_id`, `mess_file_path`, `mess_total_hits`, `mess_is_active`, `mess_user_ip`, `mess_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Chairman', 'xxxxxx', 'xxxxx', '<p>xxx</p>', '<p>xxx</p>', '<p>xxx</p>', NULL, '2018/1521715123.jpg', NULL, NULL, NULL, 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 10:38:43', '2018-03-22 10:38:43'),
(2, 'Principal', 'xxxxxx', 'xxxxx', '<p>xxxx</p>', '<p>xxxx</p>', '<p>xxxx</p>', NULL, NULL, 'xxxxx', NULL, NULL, 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 10:51:37', '2018-03-22 10:51:37'),
(3, 'Principal', 'xxxxxx', 'xxxxx', '<p>xxxx</p>', '<p>xxxx</p>', '<p>xxxx</p>', '<p>YYYY</p>', NULL, 'xxxxx', NULL, NULL, 'N', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 12:08:19', '2018-03-22 12:15:35'),
(4, 'Chairman', 'xxxxxx', 'xxxxx', '<p>vvvv</p>', '<p>vvvv</p>', '<p>vvvv</p>', NULL, '2018/1521721237.png', 'xxxxx', NULL, NULL, 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 12:20:37', '2018-03-22 12:20:37'),
(5, 'Principal', 'xxxxxx', 'xxxxx', '<p>vvvvv</p>', '<p>mmmmm</p>', '<p>vvv</p>', NULL, '2018/1521721271.png', 'xxxxx', NULL, NULL, 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 12:21:11', '2018-03-22 12:26:13'),
(6, 'Principal', 'xxxxxx', 'xxxxx', '<p>xxxx</p>', '<p>xxxxx</p>', '<p>xxxx</p>', '<p>mmmm</p>', '2018/1521721317.jpg', NULL, NULL, NULL, 'Y', '::1', '1', 'Ba04ZgKEeofMCAaeomPc1rfkCdaCUQCZtciWWAfL', '2018-03-22 12:21:57', '2018-03-22 12:24:45');

-- --------------------------------------------------------

--
-- Table structure for table `cms_notice`
--

CREATE TABLE `cms_notice` (
  `id` int(10) UNSIGNED NOT NULL,
  `noti_cat_id` varchar(19) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noti_eng_title` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noti_bng_title` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noti_eng_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `noti_bng_details` text COLLATE utf8mb4_unicode_ci,
  `noti_is_download` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noti_show_scroll` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noti_out_of_notice` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noti_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noti_file_path` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noti_total_hits` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noti_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `noti_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noti_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_notice`
--

INSERT INTO `cms_notice` (`id`, `noti_cat_id`, `noti_eng_title`, `noti_bng_title`, `noti_eng_details`, `noti_bng_details`, `noti_is_download`, `noti_show_scroll`, `noti_out_of_notice`, `noti_url`, `noti_file_path`, `noti_total_hits`, `noti_is_active`, `noti_user_ip`, `noti_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '2', 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>xxxxxxxxxxxxx</p>', '<p>xxxxxxxxxxxxxxx</p>', 'Y', 'Y', 'N', 'uuuuuuuuuuuuuuuuuuuuuuuu', '2018/2018_03_28_05_24_32_Dhaka Electric Supply Company Ltd. (DESCO).pdf', NULL, 'Y', '::1', '1', 'SulyFcrNaebCHccMPUwYi7CUPAi6LsXzwmGWh6d7', '2018-03-28 11:24:32', '2018-03-29 10:16:36'),
(2, '2', 'This is English Title', 'এটি বাংলা শিরোনাম', '<p>xxxxxxxxxxxxx</p>', '<p>xxxxxxxxxxxxxxx</p>', 'Y', 'Y', 'N', 'uuuuuuuuuuuuuuuuuuuuuuuu', '2018/2018_03_28_05_25_12_Dhaka Electric Supply Company Ltd. (DESCO).pdf', NULL, 'Y', '::1', '1', 'SulyFcrNaebCHccMPUwYi7CUPAi6LsXzwmGWh6d7', '2018-03-28 11:25:12', '2018-03-29 10:19:27');

-- --------------------------------------------------------

--
-- Table structure for table `cms_notice_category`
--

CREATE TABLE `cms_notice_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `noca_name` varchar(69) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noca_bangla_name` varchar(69) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noca_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `noca_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noca_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_notice_category`
--

INSERT INTO `cms_notice_category` (`id`, `noca_name`, `noca_bangla_name`, `noca_is_active`, `noca_user_ip`, `noca_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Office Notice', 'অামাদের কথা', 'Y', '::1', '1', 'b8jB50RVeOWfQkxVDMtOGHER1zK8SscGBwBiNEcM', '2018-03-28 05:50:59', '2018-03-28 11:45:28'),
(2, 'Departmental  Notice', 'অামাদের কথা', 'Y', '::1', '1', 'SuSDZsFbdGB0sm1TSndx8DnrqDrVWwmAJxmuIMxW', '2018-03-28 06:18:54', '2018-03-28 06:18:54');

-- --------------------------------------------------------

--
-- Table structure for table `cms_other_image`
--

CREATE TABLE `cms_other_image` (
  `id` int(10) UNSIGNED NOT NULL,
  `otim_image_path` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otim_is_slide` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `otim_show_slide` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `otim_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otim_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_other_image`
--

INSERT INTO `cms_other_image` (`id`, `otim_image_path`, `otim_is_slide`, `otim_show_slide`, `otim_user_ip`, `otim_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '2018/1522925550.jpg', 'N', 'N', '::1', '1', 'l9q6yEwmLb4k03yeOdU4IEMH0wwzjHXxgmQINXok', '2018-04-05 10:52:30', '2018-04-05 10:52:30'),
(3, '2018/1522925857.jpg', 'Y', 'N', '::1', '1', 'l9q6yEwmLb4k03yeOdU4IEMH0wwzjHXxgmQINXok', '2018-04-05 10:57:37', '2018-04-05 10:57:37'),
(4, '2018/1522926104.jpg', 'Y', 'Y', '::1', '1', 'l9q6yEwmLb4k03yeOdU4IEMH0wwzjHXxgmQINXok', '2018-04-05 11:01:44', '2018-04-05 11:01:44'),
(6, '2018/1522926191.jpg', 'N', 'N', '::1', '1', 'l9q6yEwmLb4k03yeOdU4IEMH0wwzjHXxgmQINXok', '2018-04-05 11:03:11', '2018-04-05 11:03:11');

-- --------------------------------------------------------

--
-- Table structure for table `cms_program`
--

CREATE TABLE `cms_program` (
  `id` int(10) UNSIGNED NOT NULL,
  `prog_name` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prog_bangla_name` varchar(49) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prog_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `prog_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prog_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_program`
--

INSERT INTO `cms_program` (`id`, `prog_name`, `prog_bangla_name`, `prog_is_active`, `prog_user_ip`, `prog_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'One-1', 'প্রথম শ্রেণী', 'Y', '::1', '1', 'KURsFQZYCtBX6phtyGJ3ltsk0lgrXl4FBh0pKCNB', '2018-03-15 03:49:43', '2018-04-17 03:33:57'),
(2, 'Two', NULL, 'Y', '::1', '1', 'Tp5RNTZL4LubbCqCJulT74eZ6ax3dqqdNHmw5Cpw', '2018-03-15 04:00:05', '2018-03-15 04:53:23');

-- --------------------------------------------------------

--
-- Table structure for table `cms_religion`
--

CREATE TABLE `cms_religion` (
  `id` int(10) UNSIGNED NOT NULL,
  `reli_name` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reli_bangla_name` varchar(49) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reli_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `reli_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reli_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_religion`
--

INSERT INTO `cms_religion` (`id`, `reli_name`, `reli_bangla_name`, `reli_is_active`, `reli_user_ip`, `reli_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Islam', NULL, 'Y', '::1', '1', 'Tp5RNTZL4LubbCqCJulT74eZ6ax3dqqdNHmw5Cpw', '2018-03-15 04:13:06', '2018-03-15 04:13:06');

-- --------------------------------------------------------

--
-- Table structure for table `cms_shift`
--

CREATE TABLE `cms_shift` (
  `id` int(10) UNSIGNED NOT NULL,
  `shif_name` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shif_bangla_name` varchar(49) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shif_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `shif_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shif_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_shift`
--

INSERT INTO `cms_shift` (`id`, `shif_name`, `shif_bangla_name`, `shif_is_active`, `shif_user_ip`, `shif_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Morning', NULL, 'Y', '::1', '1', 'Tp5RNTZL4LubbCqCJulT74eZ6ax3dqqdNHmw5Cpw', '2018-03-15 04:11:31', '2018-03-15 04:11:31'),
(2, 'Day', NULL, 'N', '::1', '1', 'Tp5RNTZL4LubbCqCJulT74eZ6ax3dqqdNHmw5Cpw', '2018-03-15 04:11:43', '2018-03-15 04:53:39');

-- --------------------------------------------------------

--
-- Table structure for table `cms_staff_department`
--

CREATE TABLE `cms_staff_department` (
  `id` int(10) UNSIGNED NOT NULL,
  `sfdp_name` varchar(69) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sfdp_bangla_name` varchar(69) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sfdp_is_teacher` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sfdp_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `sfdp_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sfdp_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_staff_department`
--

INSERT INTO `cms_staff_department` (`id`, `sfdp_name`, `sfdp_bangla_name`, `sfdp_is_teacher`, `sfdp_is_active`, `sfdp_user_ip`, `sfdp_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Accounting', NULL, 'Y', 'Y', '::1', '1', 'zvDSZDKhGMaxnv1xNtBlkiQl02tW7sg9TaKaXMVW', '2018-03-31 06:15:08', '2018-03-31 06:15:08'),
(2, 'Accounting', NULL, 'N', 'Y', '::1', '1', 'zvDSZDKhGMaxnv1xNtBlkiQl02tW7sg9TaKaXMVW', '2018-03-31 07:19:42', '2018-03-31 07:19:42'),
(3, 'English', NULL, 'Y', 'Y', '::1', '1', 'zvDSZDKhGMaxnv1xNtBlkiQl02tW7sg9TaKaXMVW', '2018-03-31 06:12:39', '2018-03-31 06:12:39'),
(4, 'Math', NULL, 'Y', 'Y', '::1', '1', 'zvDSZDKhGMaxnv1xNtBlkiQl02tW7sg9TaKaXMVW', '2018-03-31 06:34:07', '2018-03-31 06:34:07'),
(5, 'Male', NULL, 'N', 'Y', '::1', '1', 'zvDSZDKhGMaxnv1xNtBlkiQl02tW7sg9TaKaXMVW', '2018-03-31 07:19:36', '2018-03-31 07:19:36'),
(6, 'Ayae', NULL, 'N', 'Y', '::1', '1', 'zvDSZDKhGMaxnv1xNtBlkiQl02tW7sg9TaKaXMVW', '2018-03-31 07:19:28', '2018-03-31 07:19:28'),
(7, 'Staff-A', NULL, 'N', 'Y', '::1', '1', 'zvDSZDKhGMaxnv1xNtBlkiQl02tW7sg9TaKaXMVW', '2018-03-31 07:17:33', '2018-03-31 07:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `cms_subject`
--

CREATE TABLE `cms_subject` (
  `id` int(10) UNSIGNED NOT NULL,
  `subj_name` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subj_bangla_name` varchar(49) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subj_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `subj_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subj_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_subject`
--

INSERT INTO `cms_subject` (`id`, `subj_name`, `subj_bangla_name`, `subj_is_active`, `subj_user_ip`, `subj_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Math', NULL, 'Y', '::1', '1', 'Tp5RNTZL4LubbCqCJulT74eZ6ax3dqqdNHmw5Cpw', '2018-03-15 04:12:43', '2018-03-15 04:12:43');

-- --------------------------------------------------------

--
-- Table structure for table `cms_teacher_staff`
--

CREATE TABLE `cms_teacher_staff` (
  `id` int(10) UNSIGNED NOT NULL,
  `thsf_cat_id` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thsf_department_id` tinyint(4) DEFAULT NULL,
  `thsf_shift_id` tinyint(4) DEFAULT NULL,
  `thsf_employee_id` varchar(19) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_employee_type` varchar(17) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_designation_id` tinyint(4) DEFAULT NULL,
  `thsf_joining_date` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_seniority_id` tinyint(4) DEFAULT NULL,
  `thsf_mobile_no` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_emergency_mobile` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_email_id` varchar(49) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_fb_id` varchar(169) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_twitter_id` varchar(69) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_skype_id` varchar(69) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_google_id` varchar(69) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_linkedin_id` varchar(69) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_eng_name` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thsf_bng_name` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_father_name` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_mother_name` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_spouse_name` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_present_address` varchar(249) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_permanent_address` varchar(249) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_university_name` varchar(129) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_subject_id` tinyint(4) DEFAULT NULL,
  `thsf_religion_id` tinyint(4) DEFAULT NULL,
  `thsf_gender_id` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_married_status` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_blood_group` tinyint(2) DEFAULT NULL,
  `thsf_image_path` varchar(169) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thsf_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `thsf_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thsf_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_teacher_staff`
--

INSERT INTO `cms_teacher_staff` (`id`, `thsf_cat_id`, `thsf_department_id`, `thsf_shift_id`, `thsf_employee_id`, `thsf_employee_type`, `thsf_designation_id`, `thsf_joining_date`, `thsf_seniority_id`, `thsf_mobile_no`, `thsf_emergency_mobile`, `thsf_email_id`, `thsf_fb_id`, `thsf_twitter_id`, `thsf_skype_id`, `thsf_google_id`, `thsf_linkedin_id`, `thsf_eng_name`, `thsf_bng_name`, `thsf_father_name`, `thsf_mother_name`, `thsf_spouse_name`, `thsf_present_address`, `thsf_permanent_address`, `thsf_university_name`, `thsf_subject_id`, `thsf_religion_id`, `thsf_gender_id`, `thsf_married_status`, `thsf_blood_group`, `thsf_image_path`, `thsf_is_active`, `thsf_user_ip`, `thsf_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'S', 1, 1, 'xxxx', 'Permanent', 1, '01-01-2001', 101, '01195336679', '01195336679', 'saiful@email.com', 'xxxxx', 'xxxxx', 'xxxx', 'xxxx', 'xxxxx', 'xxxx', 'xxx', 'xxx', 'xxx', 'xxxx', 'xxxx', 'xxx', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 1, 1, 'M', 'Y', 1, '2018/1522593533.jpg', 'Y', '::1', '1', 'lLfxm8p187bpnjVMJX9aS6rt1xedgb3cZWFNFegq', '2018-04-01 14:38:53', '2018-04-01 14:38:53'),
(2, 'T', 1, 1, 'xxxx', 'Permanent', 1, '01-01-2001', 101, '01195336679', '01195336679', 'saiful@email.com', 'xxxxx', 'xxxxx', 'xxxx', 'xxxx', 'xxxxx', 'xxxx', 'xxx', 'xxx', 'xxx', 'xxxx', 'xxxx', 'xxx', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 1, 1, 'M', 'Y', 2, '2018/1522593848.jpg', 'Y', '::1', '1', 'lLfxm8p187bpnjVMJX9aS6rt1xedgb3cZWFNFegq', '2018-04-01 14:44:08', '2018-04-01 14:44:08'),
(3, 'T', 1, 1, 'xxxx', 'Permanent', 1, '01-01-2001', 101, '01195336679', '01195336679', 'saiful@email.com', 'xxxxx', 'xxxxx', 'xxxx', 'xxxx', 'xxxxx', 'xxxx', 'xxx', 'xxx', 'xxx', 'xxxx', 'xxxx', 'xxx', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 1, 1, 'M', 'Y', 3, '2018/1522584858.jpg', 'Y', '::1', '1', 'lLfxm8p187bpnjVMJX9aS6rt1xedgb3cZWFNFegq', '2018-04-01 14:44:23', '2018-04-01 14:44:23'),
(4, 'T', 1, 1, 'xxxx', 'Permanent', 1, '01-01-2001', 101, '01195336679', '01195336679', 'saiful@email.com', 'xxxxx', 'xxxxx', 'xxxx', 'xxxx', 'xxxxx', 'xxxx', 'xxx', 'xxx', 'xxx', 'xxxx', 'xxxx', 'xxx', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 1, 1, 'M', 'Y', 4, '2018/1522584858.jpg', 'Y', '::1', '1', 'lLfxm8p187bpnjVMJX9aS6rt1xedgb3cZWFNFegq', '2018-04-01 15:03:26', '2018-04-01 15:03:26'),
(5, 'T', 1, 1, 'xxxx', 'Contract Basis', 1, '01-01-2001', 101, '01195336679', '01195336679', 'saiful@email.com', 'xxxxx', 'xxxxx', 'xxxx', 'xxxx', 'xxxxx', 'xxxx', 'xxx', 'xxx', 'xxx', 'xxxx', 'xxxx', 'xxx', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 1, 1, 'M', 'Y', 5, '2018/1522595657.jpg', 'Y', '::1', '1', 'lLfxm8p187bpnjVMJX9aS6rt1xedgb3cZWFNFegq', '2018-04-01 15:14:17', '2018-04-01 15:14:17'),
(6, 'T', 1, 1, 'xxxx', 'Temporary', 1, '01-01-2001', 101, '01195336679', '01195336679', 'saiful@email.com', 'xxxxx', 'xxxxx', 'xxxx', 'xxxx', 'xxxxx', 'xxxx', 'xxx', 'xxx', 'xxx', 'xxxx', 'xxxx', 'xxx', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 1, 1, 'M', 'Y', 6, '2018/1522595623.jpg', 'Y', '::1', '1', 'lLfxm8p187bpnjVMJX9aS6rt1xedgb3cZWFNFegq', '2018-04-01 15:13:43', '2018-04-01 15:13:43');

-- --------------------------------------------------------

--
-- Table structure for table `cms_version`
--

CREATE TABLE `cms_version` (
  `id` int(10) UNSIGNED NOT NULL,
  `vers_name` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vers_bangla_name` varchar(49) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vers_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `vers_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vers_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_version`
--

INSERT INTO `cms_version` (`id`, `vers_name`, `vers_bangla_name`, `vers_is_active`, `vers_user_ip`, `vers_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Bangla', NULL, 'Y', '::1', '1', 'Tp5RNTZL4LubbCqCJulT74eZ6ax3dqqdNHmw5Cpw', '2018-03-15 04:12:09', '2018-03-15 04:12:09');

-- --------------------------------------------------------

--
-- Table structure for table `cms_video_category`
--

CREATE TABLE `cms_video_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `vict_name` varchar(69) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vict_bangla_name` varchar(69) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vict_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `vict_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vict_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_video_category`
--

INSERT INTO `cms_video_category` (`id`, `vict_name`, `vict_bangla_name`, `vict_is_active`, `vict_user_ip`, `vict_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'About Us', 'প্রথম শ্রেণী', 'Y', '::1', '1', 'ldka2vusivSZXnNLaZtlClTy0NSpgZQg2ynBWuw6', '2018-04-17 06:34:45', '2018-04-17 06:34:45'),
(2, 'Messages', 'মিরপুর', 'Y', '::1', '1', 'ldka2vusivSZXnNLaZtlClTy0NSpgZQg2ynBWuw6', '2018-04-17 06:34:58', '2018-04-17 06:34:58');

-- --------------------------------------------------------

--
-- Table structure for table `cms_video_gallery`
--

CREATE TABLE `cms_video_gallery` (
  `id` int(10) UNSIGNED NOT NULL,
  `vigl_video_cat_id` tinyint(4) NOT NULL,
  `vigl_name` varchar(69) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vigl_bangla_name` varchar(69) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vigl_video_id` varchar(69) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vigl_is_active` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `vigl_user_ip` varchar(29) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vigl_update_user` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_video_gallery`
--

INSERT INTO `cms_video_gallery` (`id`, `vigl_video_cat_id`, `vigl_name`, `vigl_bangla_name`, `vigl_video_id`, `vigl_is_active`, `vigl_user_ip`, `vigl_update_user`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'xxxxxxxx', 'xxxxxxxx', 'xxxxxxxx', 'Y', '::1', '1', 'sv1sK7a7ErDqVFG4IGjHnaqfC0IQPEbY6P0a5VLs', '2018-04-17 06:44:33', '2018-04-17 06:57:24'),
(2, 1, 'xxxxxxxx', 'xxxxxxxx', 'xxxxxxxx', 'Y', '::1', '1', 'XQFlw9M9X3h9FkLHl1c8N1ILahn2ZLLIRge40iUf', '2018-04-17 06:45:31', '2018-04-17 06:45:31'),
(3, 1, 'xxxxxxxx', 'xxxxxxxx', 'xxxxxxxx', 'Y', '::1', '1', 'XQFlw9M9X3h9FkLHl1c8N1ILahn2ZLLIRge40iUf', '2018-04-17 06:46:37', '2018-04-17 06:46:37'),
(4, 2, 'Messages', 'প্রথম শ্রেণী', 'xxx', 'Y', '::1', '1', 'OhEXSWZOtWcDH7SMOLCAclwjmiE053lMjVtPTYA2', NULL, '2018-04-18 06:22:32');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(19, '2018_03_14_105201_create_cms_program_table', 2),
(20, '2018_03_14_110008_create_cms_subject_table', 2),
(21, '2018_03_14_110251_create_cms_group_table', 2),
(22, '2018_03_14_110440_create_cms_shift_table', 2),
(23, '2018_03_14_110628_create_cms_designation_table', 2),
(24, '2018_03_14_111403_create_cms_religion_table', 2),
(25, '2018_03_14_111524_create_cms_board_table', 2),
(26, '2018_03_14_111640_create_cms_version_table', 2),
(27, '2018_03_15_142510_create_cms_contents_category', 3),
(30, '2018_03_15_164406_create_cms_contents_master', 4),
(32, '2018_03_22_154313_create_cms_messages', 5),
(33, '2018_03_28_111445_create_cms_notice_category', 6),
(35, '2018_03_28_144202_create_cms_notice', 7),
(36, '2018_03_31_112739_create_cms_staff_department', 8),
(38, '2018_03_31_164038_create_cms_teacher_staff', 9),
(39, '2018_04_01_200333_create_cms_blood_group', 10),
(40, '2018_04_03_105403_create_cms_album_category', 11),
(42, '2018_04_03_105423_create_cms_album_group', 12),
(43, '2018_04_04_123200_create_cms_album_image', 13),
(44, '2018_04_05_160159_create_cms_other_image', 14),
(47, '2018_04_17_105222_create_cms_video_category', 15),
(48, '2018_04_17_113129_create_cms_video_gallery', 15);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(69) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `institute_name` varchar(169) CHARACTER SET utf8 DEFAULT NULL,
  `user_type` varchar(69) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'EdiToR',
  `user_reference` int(9) NOT NULL,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `ip_address` varchar(69) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `designation`, `email`, `mobile`, `password`, `institute_name`, `user_type`, `user_reference`, `is_active`, `ip_address`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mohammad Saiful Islam', NULL, 'saiful@email.com', NULL, '$2y$10$NW69ImJesnH.iUQMqoD1n.y/Z9DL4wjZoKjg2VuA9TS1SaNRyFVuW', 'Desh Universal Privet Limited', 'SuperMan', 0, 'Y', '', 'pAS4BEWav8QEyhcn8vFooaWAmux1c54J1FaCehxyBdbssutYUotDcPn1jHZs', '2018-01-28 23:09:53', '2018-01-28 23:09:53'),
(2, 'Md. Mainuddin', 'Server Administrator', 'mainuddin@email.com', '01195336677', '$2y$10$jwyEkd/ZxumwW4rsDW1iEOgfhbdkaSEfoZNKwZmxSwJxZfwF2FU4y', NULL, 'IroNMaN', 1, '1', '::1', '2UE28WkXe6RpUmJuWEPTvxfArG1FAt7rkbjsVORw22ved1zuK4qdUmzv2Yz0', '2018-01-30 11:04:49', '2018-01-30 11:04:49'),
(3, 'Mr.Molin', 'Programmer', 'molin@email.com', '01195336678', '$2y$10$tonE2Dw26yZx2k1UtrjN7..ZmYRRShD.l.0xYDoQr17Bh0ln53HtK', NULL, 'EdiToR', 2, '1', '::1', 'vRxbSQkOdVhoFXeikouXx5S9LMP1wwibgvfveWiG', '2018-01-30 11:09:14', '2018-01-30 11:09:14'),
(4, 'Mr. Mahabub Rohoman', 'Suport Officer', 'mahabub@email.com', '01195336679', '$2y$10$BZneI6AkO3Hp2BrKBclp0.r8wGjSEMdea7EDpIyUbs.bsYZ3P3lL6', NULL, 'EdiToR', 2, '2', '::1', '9BytNnjfxyeYh1uWWsctM4LTEnztF6BtwHFx6Jz2', '2018-01-30 11:13:10', '2018-01-30 11:18:39'),
(5, 'Mr. Jashim', 'Sr. Programmer', 'jashim@email.com', '011953366710', '$2y$10$sukPam.yjiUvOwMqWnPMMeiB7od/6Ze7r80awYviZoUWWvQ/9y7fK', NULL, 'EdiToR', 2, '1', '::1', '9BytNnjfxyeYh1uWWsctM4LTEnztF6BtwHFx6Jz2', '2018-01-30 11:25:30', '2018-01-30 11:25:30'),
(6, 'hasib', NULL, 'ghhgh@mail.com', NULL, '$2y$10$Ww/DfOryH6a9CeEYaID6neqBYpVgxgaiu0AAT5jsgSXQj3urOMK0a', NULL, 'EdiToR', 1, '1', '::1', 'DFIN0MIkRYsQOQcMm9xsRJEphqxAiuvFMxwD3FIk', '2018-03-07 10:50:51', '2018-03-07 10:50:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_album_category`
--
ALTER TABLE `cms_album_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_album_group`
--
ALTER TABLE `cms_album_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_album_image`
--
ALTER TABLE `cms_album_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_blood_group`
--
ALTER TABLE `cms_blood_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_board`
--
ALTER TABLE `cms_board`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_contents_category`
--
ALTER TABLE `cms_contents_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_contents_master`
--
ALTER TABLE `cms_contents_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_designation`
--
ALTER TABLE `cms_designation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_group`
--
ALTER TABLE `cms_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_messages`
--
ALTER TABLE `cms_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_notice`
--
ALTER TABLE `cms_notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_notice_category`
--
ALTER TABLE `cms_notice_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_other_image`
--
ALTER TABLE `cms_other_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_program`
--
ALTER TABLE `cms_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_religion`
--
ALTER TABLE `cms_religion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_shift`
--
ALTER TABLE `cms_shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_staff_department`
--
ALTER TABLE `cms_staff_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_subject`
--
ALTER TABLE `cms_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_teacher_staff`
--
ALTER TABLE `cms_teacher_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_version`
--
ALTER TABLE `cms_version`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_video_category`
--
ALTER TABLE `cms_video_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_video_gallery`
--
ALTER TABLE `cms_video_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_album_category`
--
ALTER TABLE `cms_album_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `cms_album_group`
--
ALTER TABLE `cms_album_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cms_album_image`
--
ALTER TABLE `cms_album_image`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cms_blood_group`
--
ALTER TABLE `cms_blood_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `cms_board`
--
ALTER TABLE `cms_board`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_contents_category`
--
ALTER TABLE `cms_contents_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cms_contents_master`
--
ALTER TABLE `cms_contents_master`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `cms_designation`
--
ALTER TABLE `cms_designation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_group`
--
ALTER TABLE `cms_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_messages`
--
ALTER TABLE `cms_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cms_notice`
--
ALTER TABLE `cms_notice`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_notice_category`
--
ALTER TABLE `cms_notice_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_other_image`
--
ALTER TABLE `cms_other_image`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cms_program`
--
ALTER TABLE `cms_program`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_religion`
--
ALTER TABLE `cms_religion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_shift`
--
ALTER TABLE `cms_shift`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_staff_department`
--
ALTER TABLE `cms_staff_department`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `cms_subject`
--
ALTER TABLE `cms_subject`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_teacher_staff`
--
ALTER TABLE `cms_teacher_staff`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cms_version`
--
ALTER TABLE `cms_version`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_video_category`
--
ALTER TABLE `cms_video_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_video_gallery`
--
ALTER TABLE `cms_video_gallery`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
