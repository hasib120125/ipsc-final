<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){

    	return view('index');
    }

    public function atAglance(){

    	return view('at-a-glance'); 
    }

    public function newsEvents(){

    	return view('news-events');
    }

    public function governingBody(){

    	return view('governing-body');
    }

    public function messages(){

    	return view('messages');
    }

    public function teacherStaff(){

    	return view('teacher-staff');
    }

    public function academic(){

    	return view('academic');
    }

    public function notice(){

    	return view('notice');
    }

    public function admission(){

    	return view('admission');
    }

    public function photoGallery(){

    	return view('photo-gallery');
    }

    public function videoGallery(){

    	return view('video-gallery');
    }
}

