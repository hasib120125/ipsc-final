<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Frontend Routes Start

Route::get('/', 'HomeController@index');

Route::get('/at-a-glance', 'HomeController@atAglance');

Route::get('/why-study-ipsc', 'HomeController@whyStudyIpsc');

Route::get('/history', 'HomeController@history');

Route::get('/infrastructure', 'HomeController@infrastructure');

Route::get('/achievement', 'HomeController@achievement');

Route::get('/news-events', 'HomeController@newsEvents');

Route::get('/governing-body', 'HomeController@governingBody');

Route::get('/messages', 'HomeController@messages');

Route::get('/teacher-staff', 'HomeController@teacherStaff');

Route::get('/academic', 'HomeController@academic');

Route::get('/notice', 'HomeController@notice');

Route::get('/admission', 'HomeController@admission');

Route::get('/facilities', 'HomeController@facilities');

Route::get('/co-curricular', 'HomeController@coCurricular');

Route::get('/club', 'HomeController@club');

Route::get('/photo-gallery', 'HomeController@photoGallery');

Route::get('/photo-album', 'HomeController@photoGallery');

Route::get('/photo-details', 'HomeController@photoGallery');

Route::get('/video-gallery', 'HomeController@videoGallery');

Route::get('/video-details.php', 'HomeController@videoGallery');

//Frontend Routes End