<div class="container-fluid">
    <div class="row footer-row-one div-hid">
        <div class="container">
            <div class="col-sm-4 padding-top5p home-gallery">
                <h3 style="border-bottom: 1px solid #ddd">Photo Gallery</h3>
                <ul>
                    <li>
                        <a href="gallery.php">
                            <img src="<?php echo e(asset('frontend/images/school-image2.jpg')); ?>" height="50" width="50">
                        </a>
                    </li>
                </ul>

                <ul>
                    <li>
                        <a href="gallery.php">
                            <img src="<?php echo e(asset('frontend/images/school-image2.jpg')); ?>" height="50" width="50">
                        </a>
                    </li>
                </ul>

                <ul>
                    <li>
                        <a href="gallery.php">
                            <img src="<?php echo e(asset('frontend/images/school-image2.jpg')); ?>" height="50" width="50">
                        </a>
                    </li>
                </ul>
                
            </div>

            <div class="col-sm-4 footer-center" align="center" style="">
                <img src="<?php echo e(asset('frontend/images/logo.png')); ?>" class="img-responsive">
            </div>
            
            <div class="col-sm-4 padding-top5p home-gallery">
                <h3 style="border-bottom: 1px solid #ddd">Video Gallery</h3>
                <ul>
                    <li>
                        <a href="gallery.php">
                            <img src="<?php echo e(asset('frontend/images/school-image2.jpg')); ?>" height="50" width="50">
                        </a>
                    </li>
                </ul>

                <ul>
                    <li>
                        <a href="gallery.php">
                            <img src="<?php echo e(asset('frontend/images/school-image2.jpg')); ?>" height="50" width="50">
                        </a>
                    </li>
                </ul>

                <ul>
                    <li>
                        <a href="gallery.php">
                            <img src="<?php echo e(asset('frontend/images/school-image2.jpg')); ?>" height="50" width="50">
                        </a>
                    </li>
                </ul>
                
            </div>

        </div>
    </div>

    <div class="row div-hid footer-row-two">
        <div class="container">
            <div class="col-sm-4 padding-top5p quick_link">
                <h3 style="border-bottom: 1px solid #ddd">Quick Link</h3>
                <ul class="list-unstyled margin-bottom11">
                    <li><a href="http://www.educationboard.gov.bd/" target="_blank"><i class="fa fa-anchor" aria-hidden="true"></i> Education Board</a></li>
                    <li><a href="http://www.educationboard.gov.bd/" target="_blank"><i class="fa fa-anchor" aria-hidden="true"></i> Dhaka Education Board</a></li>
                    <li><a href="http://www.moedu.gov.bd/" target="_blank"><i class="fa fa-anchor" aria-hidden="true"></i> Ministry of Education</a></li>
                    <li><a href="http://banbeis.gov.bd/new/" target="_blank"><i class="fa fa-anchor" aria-hidden="true"></i> Banbeis</a></li>
                    <li><a href="http://www.dshe.gov.bd/" target="_blank"><i class="fa fa-anchor" aria-hidden="true"></i> Directorate of Secondary & Higher Education</a></li>
                </ul>
            </div>
            <div class="col-sm-4 footer-center-bottom" align="center">
                <span>Ispahani<br> Public School & College</span>
            </div>
            
            <div class="col-sm-4 padding-top5p">
                <h3 style="border-bottom: 1px solid #ddd"><i class="fa fa-line-chart" aria-hidden="true"></i> Visitor Statistics</h3>
                <?php echo $__env->make('frontend/include/counter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>

        </div>
    </div>

    <div class="row footer-bottom-left hidden">
        <div class="col-xs-12 col-sm-6">
        </div>
    </div>

    <div class="row footer-bottom"><div class="container" align="center" style="color: #fff">&copy; <?php echo date("Y");?> - Ispahani Public School & College All Rights Reserved. Developed by <a href="http://deshuniversal.com/" target="blank" style="color:#fdbe0c;">Desh Universal (Pvt.) Limited.</a> <a href="https://www.facebook.com/deshuniversal/" target="_blank"> Like us on <span style="color:#f7ac23;">Facebook</span></a></div></div>
</div>