<div class="container-fluid contact-bg">
    <div class="row">
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-12 contact-bg-img contact">
              <h2>Contact Us </h2><hr>
              <h3 style="padding-bottom: 8px;">Ispahani Public School & College </h3>
              <h3 style="padding-bottom: 8px;">Comilla Cantonment 3501</h3>
              <h3 style="padding-bottom: 8px;">Phone : +880 817-6766</h3>
              <h3 style="padding-bottom: 8px;">Email : ipsccml@gmail.com</h3>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12 google-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3659.5479476790024!2d91.11728921465436!3d23.47676518472499!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x37547bf45642b5af%3A0x9760ec0d98f2bcdc!2z4KaH4Ka44KeN4Kaq4Ka-4Ka54Ka-4Kao4KeAIOCmquCmvuCmrOCmsuCmv-CmlSDgprjgp43gppXgp4HgprIg4KaP4Kao4KeN4KahIOCmleCmsuCnh-CmnA!5e0!3m2!1sbn!2sbd!4v1522665596358" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
</div>