<div class="row margin-top5P">
    <div class="col-xs-4 col-sm-4 padding-left0 padding-right0" style="padding-right: 25px;">
      <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class='img-responsive img-thumbnail img-height'     width: 116px;>
    </div>
    <div class="col-xs-8 col-sm-8 padding-left0">
      <div class="row events-date">Jun 1, 2016</div>
      <div class="row"><h2 class="events-title">The Arrival of the Honorable New Principal</h2></div>
      <div class="row events-brif">
         <a class='a-coral' href="#"> Read more...</a>
      </div>
  </div>
</div>

<div class="row margin-top5P">
  <div class="col-xs-4 col-sm-4 padding-left0 padding-right0" style="padding-right: 25px;">
    <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class='img-responsive img-thumbnail img-height'>
  </div>
  <div class="col-xs-8 col-sm-8 padding-left0">
    <div class="row events-date">Jul 21, 2016</div>
    <div class="row"><h2 class="events-title">The Opening of Auditorium Building</h2></div>
    <div class="row events-brif">
       <a class='a-coral' href='news-events.php'> Read more...</a>
    </div>
  </div>
</div>

<div class="row margin-top5P">
  <div class="col-xs-4 col-sm-4 padding-left0 padding-right0" style="padding-right: 25px;">
    <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class='img-responsive img-thumbnail img-height'>
  </div>
  <div class="col-xs-8 col-sm-8 padding-left0">
    <div class="row events-date">Jul 21, 2016</div>
    <div class="row"><h2 class="events-title">EDUCATION MINISTER VISITS OUR COLLEGE</h2></div>
    <div class="row events-brif">
       <a class='a-coral' href="#"> Read more...</a>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-xs-4 col-sm-4">
  </div>
  <div class="col-xs-8 col-sm-8 padding-left0" style="text-align: right;padding-bottom: 2px;">
    <a href="#"><button type="button" class="btn-default" style="padding: 5px;width: 114px;color: #ff6b7c;">View All</button></a>
  </div>
</div>