<?php echo $__env->make('include.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="container-fluid body-bg">
    <div class="container margin-top10">
        <div class="row">
            <div class="col-sm-8">
                <div class="row top-head">
                    <h1> Ispahani Public School & College Governing Body</h1>
                </div>

                <div class="row">
                    <div class="col-sm-4" align="center"></div>
                    <div class="col-sm-4" align="center">
                        <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class="gb-height img-responsive margin-bottom2P margin-right10" alt="" title="">
                        <span class="gb-name">Chairman Name</span><br>
                        <span>Chairman, Governing Body<br>
                        Ispahani Public School & College</span>
                    </div>
                    <div class="col-sm-4" align="center"></div>
                </div>

                <div class="row margin-top5P">
                    <div class="col-sm-4" align="center">
                        <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class="gb-height img-responsive margin-bottom2P margin-right10" alt="" title="">
                        <span class="gb-name">Vice Chairman Name</span><br>
                        <span>Vice Chairman<br>
                        Ispahani Public School & College Governing Body</span>
                    </div>
                    <div class="col-sm-4" align="center">
                        <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class="gb-height img-responsive margin-bottom2P margin-right10" alt="" title="">
                        <span class="gb-name">Member Name</span><br>
                        <span>Member<br>
                        Governing Body. Additional Director (Education) </span>
                    </div>
                    <div class="col-sm-4" align="center">
                        <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class="gb-height img-responsive margin-bottom2P margin-right10" alt="" title="">
                        <span class="gb-name">Member Name</span><br>
                        <span>Member<br>
                        Assistant Professor, Bangla</span>

                    </div>
                </div>

                <div class="row margin-top5P margin-bottom20">
                    <div class="col-sm-4" align="center">
                        <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class="gb-height img-responsive margin-bottom2P margin-right10" alt="" title="">
                        <span class="gb-name">Member Name</span><br>
                        <span>Member<br>
                        Director ( Admin& welfare) Deputy Secretary Bangla</span>
                    </div>
                    <div class="col-sm-4" align="center">
                        <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class="gb-height img-responsive margin-bottom2P margin-right10" alt="" title="">
                        <span class="gb-name">Member Name</span><br>
                        <span>Member<br>
                        Deputy Secretary Finance Division Ministery of finance.</span>
                    </div>
                    <div class="col-sm-4" align="center">
                        <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class="gb-height img-responsive margin-bottom2P margin-right10" alt="" title="">
                        <span class="gb-name">Member Name</span><br>
                        <span>Member<br>
                        Assistant Professor, Bangla</span>
                    </div>
                </div>

            </div>

            <div class="col-sm-4">
                <div class="col-xs-12 col-sm-12">
                    <div class="row top-head-right">
                        <a href="#"><i class="fa fa-home" aria-hidden="true"></i></a> / Administration / Governing Body
                    </div>

                    <div class="row right-video">
                        <i class="fa fa-file-video-o" aria-hidden="true"></i> Suggested Video
                    </div>
                    <div class="row margin-bottom20">
                        <iframe width="360" height="195" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe>
                    </div>

                    <div class="row related-topics box-shado">
                        <i class="fa fa-windows" aria-hidden="true"></i> Related Topics
                    </div>
                    <div class="row margin-bottom20">
                        <?php echo $__env->make("aside/administration-aside", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<?php echo $__env->make('include.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>