<?php $__env->startSection('slider'); ?>
<div class="item active"><div class="fill" style="background-image:url(<?php echo e(asset('frontend/images/slide/slider3.jpg')); ?>);"></div>
    <div class="carousel-caption">
        <h2></h2>
    </div>
</div>

<div class="item"><div class="fill" style="background-image:url(<?php echo e(asset('frontend/images/slide/slider6.jpg')); ?>);"></div>
    <div class="carousel-caption">
        <h2></h2>
    </div>
</div>

<div class="item"><div class="fill" style="background-image:url(<?php echo e(asset('frontend/images/slide/slider7.jpg')); ?>);"></div>
    <div class="carousel-caption">
        <h2></h2>
    </div>
</div>

<div class="item"><div class="fill" style="background-image:url(<?php echo e(asset('frontend/images/slide/slider11.jpg')); ?>);"></div>
    <div class="carousel-caption">
        <h2></h2>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('why-study'); ?>
    <div class="col-sm-6 slide-bottom-left" align="center" style="position: static !important;">
      <h2 class="h2" style="margin: 0px !important">Why Study at <span>IPSC</span></h2>
      <p class="txt-color">The institution offers a good number of co-curricular activities such as. B CC army (male and female), BNCC navy (male) B CC Air (Male and Female), Army (Junior) Holde Pakhi, Rover Scouts (Air, Boys, Girls) Cub Scout, Air Scout, Navy Scout, Band group.English and Debate, Music, Dance, Display, Football, Volleyball, Basketball. </p>
        <a class="btn btn-primary margin-top7" href="why_study_en.php">Read More</a>
    </div>

    <div class="col-sm-6 slide-bottom-right" style="position: static !important;float: right;color: #000;background: #003B6E;">
      <div class="col-sm-2 img-dedication"><img src="<?php echo e(asset('frontend/images/ipsc.png')); ?>" class="img-responsive"></div>
      <div class="col-sm-10">
        <h2 class="h2">Dedication</h2>
        <p class="txt-color">১৯৬৫ সালের জুন মাসে ময়নামতি পাবলিক স্কুল হিসাবে নামকরণ করা হয়।[৩] ১৯৬৬ সালে ২৮শে সেপ্টেম্বর দানবীর মির্জা আহমেদ ইস্পাহানীর আর্থিক সহায়তায় গ্রহণের পর প্রতিষ্ঠানটির নাম ইস্পাহানী পাবলিক স্কুল নাম করন করা হয়। পরে ১৯৭৫ সালের ২৮শে অক্টোবর প্রতিষ্ঠানটিতে কলেজ শাখা করার পর প্রতিষ্ঠানটির সর্বশেষ নাম করন করা হয় ইস্পাহানী পাবলিক স্কুল এন্ড কলেজ।</p>
        <a class="btn btn-primary margin-top7" href="details.php">Read More</a>
      </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('about-us'); ?>
<div class="row text-justify">
	<p>Ispahani Public School & College, Comilla commonly referred to as IPSC is a well known private educational institution on Comilla,Bangladesh. The institution offers primary, secondary and higher secondary education facilities under the Comilla Education Board. Founded by Mirza Ahmad Ispahani in 1979 the institution is widely known for its excellent results in S.S.C. and H.S.C. exams and its discipline.
	</p>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('message-chairman'); ?>
<div class="col-sm-4">
    <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class="img-responsive">
    <p>Chief Patrom Name</p>
    <a href="messages.php" class="btn btn-primary margin-top5P margin-bottom10">Read Message</a>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('message-vice-chairman'); ?>
<div class="col-sm-5">
	<span class="msg-text">Message from Chairman</span>
    <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class="img-responsive">
    <p>Chairman Name</p>
    <a href="messages.php" class="btn btn-primary margin-top5P">Read Message</a>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('message-principle'); ?>
<div class="col-sm-4">
	<span class="msg-text">Message from Principal</span>
    <img src="<?php echo e(asset('frontend/images/professor.jpg')); ?>" class="img-responsive">
    <p>Principle Name</p>
    <a href="messages.php" class="btn btn-primary margin-top5P">Read Message</a>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('at-a-glance'); ?>
<div class="container-fluid">
	<div class="container">
	  <div class="row">
	    <div class="col-sm-4">
	      <div class="home-life-side-img" style="background-image: url(<?php echo e(asset('frontend/images/home_right.jpg')); ?>)"></div>
	    </div>

	    <div class="col-sm-4" style="padding-top: 100px;">
	      <div class="home-life" align="center">
	        <span class="about-text">At a Glance</span>
	          	<div class="row margin-top5P">
				   <p>The organization, named Cantonment Public School, started on 28 September 1962 and started its education program at present Comilla Zilla School. In 1963, the school was shifted to the current Cantonment Board School. Initially, SSC examination was conducted in this school in 1964.
				</div>
	          <a href="about_details.php" class="btn btn-primary margin-top5P">Read More</a>
	      </div>
	    </div>

	    <div class="col-sm-4">
	      <div class="home-life-side-img" style="background-image: url(<?php echo e(asset('frontend/images/home_left.jpg')); ?>)">
	      </div>
	    </div>
	  </div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('home-result'); ?>
<div class="container-fluid" style="padding-top: 28px;padding-bottom: 100px;">
   <div class="container">
      <h4 class="result-head" style="text-align: center">Institute Results</h4>
    <div class="row">
      <div class="col-sm-12">
        <div class="container" align="center">
                   <ul class="nav nav-tabs margin-top10">
                       <li><a data-toggle="tab" href="#menu1" style="background-color: #fff; color: #000 !important">PSC</a></li>
                       <li><a data-toggle="tab" href="#menu2" style="background-color: #fff; color: #000 !important">JSC</a></li>
                       <li><a data-toggle="tab" href="#menu3" style="background-color: #fff; color: #000 !important">SSC</a></li>
                       <li><a data-toggle="tab" href="#menu4" style="background-color: #fff; color: #000 !important">HSC</a></li>
                   </ul>

               <div class="tab-content">

                   <div id="menu1" class="tab-pane fade">
                       <h3>PSC Result</h3>
                       <h4>IPSC</h4>
                       <br>
                       <a href="academic.php" class="btn btn-info" target="_blank">Read More</a>
                   </div>

                   <div id="menu2" class="tab-pane fade">
                       <h3>JSC Result</h3>
                       <h4>IPSC</h4>
                       <br>
                       <a href="academic.php" class="btn btn-info" target="_blank">Read More</a>
                   </div>

                   <div id="menu3" class="tab-pane fade">
                       <h3>SSC Result</h3>
                      
                       <h4>IPSC</h4>
                       <br>
                       <a href="academic.php" class="btn btn-info" target="_blank">Read More</a>
                   </div>

                   <div id="menu4" class="tab-pane fade">
                       <h3>HSC Result</h3>
                      
                       <h4>IPSC</h4>
                       <br>
                       <a href="academic.php" class="btn btn-info" target="_blank">Read More</a>
                   </div>
               </div>
           </div>
      </div>
      </div>
   </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('home-notice'); ?>
<div class="container-fluid">
	<div class="row">
	  <div class="col-sm-6">
	    <div class="row video-bg" align="center">
	      <div class="col-sm-12" style="padding-top: 17px;">
	        <h2 class="video-title">Our Featured Video</h2>
	        <p class="video-text">Conservators in Collections Care repair a book with a broken cover to prevent further damage. Protecting Harvard’s special collections of rare books, manuscripts, prints, drawings, maps, photographs, and other treasures is the mission of Harvard Library Preservation Services.</p>
	      </div>

	      <div class="col-xs-12 col-sm-12" style="padding-top: 6px;">
	        <iframe class="iframe-wh" src="https://www.youtube.com/embed/iu-naJgj3I8" frameborder="0" allowfullscreen></iframe>
	      </div>

	      <a href="video-gallery.php" class="btn btn-primary margin-top5P margin-bottom2P">WATCH MORE VIDEO</a>
	    </div>

	    <div class="row fb-bg">
	      <div class="col-xs-12 col-sm-12" align="center">
	          <h2 class="fb-title">Facebook Official Fan Page</h2>
	          <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fipsc.ispahanian%2F&tabs=timeline&width=350&height=181&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId" width="350" height="181" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
	      </div>
	    </div>
	  </div>



	  <div class="col-sm-6">
	    <div class="row notice-bg">
	      <div class="col-sm-12">
	          <div class="row notice-margin">
	              <div class="col-sm-12">
	              <span class="glance-text">Latest Notice</span>
	              </div>
	          </div>
	          <div class="row">
	              <div class="col-sm-12">
	                  <div class="row notice-border">
					  <div class="col-xs-3 col-sm-3">
					      <div class="col-sm-12 date-month" align="center">May</div>
					      <div class="col-sm-12 date-day" align="center">14</div>
					  </div>
					  <div class="col-xs-9 col-sm-9">
					    <div class="row">
					     <span class="notice-title">Class XI Fees 2017-18 </span><br>
						 <span class="notice-department">Department :</span>XI Class
					  	</div>
						 <div class="row padding-top5p">
						 <a href="notice_details.php" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
						 <a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
						 </div>
					  </div>
					</div>

					<div class="row notice-border">
					  <div class="col-xs-3 col-sm-3">
					      <div class="col-sm-12 date-month" align="center">May</div>
					      <div class="col-sm-12 date-day" align="center">9</div>
					  </div>
					  <div class="col-xs-9 col-sm-9">
					     <div class="row">
					     <span class="notice-title">Class XI Admission Circular</span><br>
						 <span class="notice-department">Department :</span>College
					  	 </div>
						 <div class="row padding-top5p">
						 <a href="notice_details.php" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
						 <a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
						 </div>
					  </div>
					</div>

					<div class="row notice-border">
					  <div class="col-xs-3 col-sm-3">
					      <div class="col-sm-12 date-month" align="center">May</div>
					      <div class="col-sm-12 date-day" align="center">5</div>
					  </div>
					  <div class="col-xs-9 col-sm-9">
					     <div class="row">
					     <span class="notice-title">Recruitment Notice - 2017</span><br>
						 <span class="notice-department">Department :</span>Office
					  	 </div>
						 <div class="row padding-top5p">
						 <a href="notice_details.php" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
						 <a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
						 </div>
					  </div>
					</div>

					<div class="row notice-border">
					  <div class="col-xs-3 col-sm-3">
					      <div class="col-sm-12 date-month" align="center">Apr</div>
					      <div class="col-sm-12 date-day" align="center">28</div>
					  </div>
					  <div class="col-xs-9 col-sm-9">
					     <div class="row">
					     <span class="notice-title">Recruitment Notice - 2017</span><br>
						 <span class="notice-department">Department :</span>Office
					  	 </div>
						 <div class="row padding-top5p">
						 <a href="notice_details.php" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
						 <a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
						 </div>
					  </div>
					</div>

					<div class="row notice-border">
					  <div class="col-xs-3 col-sm-3">
					      <div class="col-sm-12 date-month" align="center">Mar</div>
					      <div class="col-sm-12 date-day" align="center">27</div>
					  </div>
					  <div class="col-xs-9 col-sm-9">
					     <div class="row">
					     <span class="notice-title">Principal BMARPC Facebook Page.</span><br>
						 <span class="notice-department">Department :</span>Office
					  	 </div>
						 <div class="row padding-top5p">
						 <a href="notice_details.php" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
						 <a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
						 </div>
					  </div>
					</div>

					<div class="row notice-border">
						<div class="col-xs-3 col-sm-3">
						    <div class="col-sm-12 date-month" align="center">Feb</div>
						    <div class="col-sm-12 date-day" align="center">5</div>
						</div>
					  	<div class="col-xs-9 col-sm-9">
					     	<div class="row">
					     	<span class="notice-title">Class Eleven 2nd CT & Class Twelve Evaluation Exam Corrected Routine</span><br>
							<span class="notice-department">Department :</span>College
					  		</div>
							<div class="row padding-top5p">
							 	<a href="notice_details.php" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
							 	<a href="notice-download?file=05-02-2017-1486282916.pdf"><i class="fa fa-download" aria-hidden="true"></i></a>
							</div>
					  	</div>
					</div>
	              </div>
	          </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('home-news-events'); ?>
<div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <div class="row achivement-bg">
          <div class="col-sm-12 margin-top5P">
              <div class="col-sm-3"></div>
              <div class="col-sm-9"><span class="achivement-head">Achivements</span></div>
          </div>

          <div class="col-sm-12 margin-top5P">
              <div class="col-sm-9">
                  <div class="row margin-top5P" >
					  <div class="col-xs-4 col-sm-4" style="padding-right: 25px;padding-left: 0px">
					    <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class='img-responsive img-thumbnail img-height'>
					  </div>
					  <div class="col-xs-8 col-sm-8 padding-left0">
					    <div class="row achivement-date">Dec 5, 2015</div>
					    <div class="row"><h2 class="achivement-title">7th Debating Festival of Anthropology at the University of Dhaka</h2></div>
					    <div class="row achivement-brif">
					       <a href='achievements.php'> Read more...</a>
					    </div>
					  </div>
					</div>

					<div class="row margin-top5P" >
					  <div class="col-xs-4 col-sm-4" style="padding-right: 25px;padding-left: 0px">
					    <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class='img-responsive img-thumbnail img-height'>
					  </div>
					  <div class="col-xs-8 col-sm-8 padding-left0">
					    <div class="row achivement-date">Dec 23, 2015</div>
					    <div class="row"><h2 class="achivement-title">Royal Cement - College Debating Competition</h2></div>
					    <div class="row achivement-brif">
					       <a href='achievements.php'> Read more...</a>
					    </div>
					  </div>
					</div>

					<div class="row margin-top5P" >
					  <div class="col-xs-4 col-sm-4" style="padding-right: 25px;padding-left: 0px">
					    <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class='img-responsive img-thumbnail img-height'>
					  </div>
					  <div class="col-xs-8 col-sm-8 padding-left0">
					    <div class="row achivement-date">Dec 7, 2015</div>
					    <div class="row"><h2 class="achivement-title">CSE Festival organized by the World University</h2></div>
					    <div class="row achivement-brif">
					       <a href='achievements.php'> Read more...</a>
					    </div>
					  </div>
					</div>

					<div class="row">
					  <div class="col-xs-4 col-sm-4">
					  </div>
					  <div class="col-xs-8 col-sm-8 padding-left0" style="text-align: right;padding-bottom: 2px;">
					    <a href="achievements.php"><button type="button" class="btn-default" style="padding: 5px;width: 114px;color: #ff6b7c;">View All</button></a>
					  </div>
					</div>
              </div>
          </div>
        </div>
      </div>

      <div class="col-sm-6">
        <div class="row events-bg">
          <div class="col-sm-12 margin-top5P">
              <div class="col-sm-9"><span class="events-head">Events</span></div>
              <div class="col-sm-3"></div>
          </div>

          <div class="col-sm-12 margin-top5P margin-left5P">
              	<div class="col-sm-9">
                  	<div class="row margin-top5P">
					    <div class="col-xs-4 col-sm-4 padding-left0 padding-right0" style="padding-right: 25px;">
					      <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class='img-responsive img-thumbnail img-height'     width: 116px;>
					    </div>
					    <div class="col-xs-8 col-sm-8 padding-left0">
						    <div class="row events-date">Jun 1, 2016</div>
						    <div class="row"><h2 class="events-title">The Arrival of the Honorable New Principal</h2></div>
					      	<div class="row events-brif">
					         	<a class='a-coral' href='news-events.php'> Read more...</a>
					      	</div>
					  	</div>
					</div>

					<div class="row margin-top5P">
						<div class="col-xs-4 col-sm-4 padding-left0 padding-right0" style="padding-right: 25px;">
							<img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class='img-responsive img-thumbnail img-height'>
						</div>
						<div class="col-xs-8 col-sm-8 padding-left0">
						    <div class="row events-date">Jul 21, 2016</div>
						    <div class="row"><h2 class="events-title">The Opening of Auditorium Building</h2></div>
						    <div class="row events-brif">
						       <a class='a-coral' href='news-events.php'> Read more...</a>
						    </div>
						</div>
					</div>

					<div class="row margin-top5P">
						<div class="col-xs-4 col-sm-4 padding-left0 padding-right0" style="padding-right: 25px;">
						    <img src="<?php echo e(asset('frontend/images/vc.png')); ?>" class='img-responsive img-thumbnail img-height'>
						</div>
					  	<div class="col-xs-8 col-sm-8 padding-left0">
						    <div class="row events-date">Jul 21, 2016</div>
						    <div class="row"><h2 class="events-title">EDUCATION MINISTER VISITS OUR COLLEGE</h2></div>
						    <div class="row events-brif">
						       <a class='a-coral' href='news-events.php'> Read more...</a>
						    </div>
					  	</div>
					</div>


					<div class="row">
					  <div class="col-xs-4 col-sm-4">
					  </div>
					  <div class="col-xs-8 col-sm-8 padding-left0" style="text-align: right;padding-bottom: 2px;">
					    <a href="news-events.php"><button type="button" class="btn-default" style="padding: 5px;width: 114px;color: #ff6b7c;">View All</button></a>
					  </div>
					</div>
              	</div>

              <div class="col-sm-3"></div>
          </div>
        </div>
      </div>
    </div>
 </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>