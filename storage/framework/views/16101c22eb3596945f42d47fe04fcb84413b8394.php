<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <title>Ispahani</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="description" content="School and College from Bangladesh.">
  <meta name="keywords" content="School, College, University, Education, Result, Class Room, Exam, Notice, Routine, Rouf College, Rifles College, Principal, Chairman, Teacher , Staff">
  <meta name="author" content="#">
  <meta http-equiv="refresh" content="600">
  <meta name="robots" content="all">
  <meta name="googlebot" content="all">
  <meta name="googlebot-news" content="all">
  <meta name="rating" content="safe for kids">
  <link rel="canonical" href="#">
  <link type="image/x-icon" rel="shortcut icon" href="#">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/bootstrap.min.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/animate.min.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/bootstrap-dropdownhover.min.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/bootstrap-theme.min.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/full-slider.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/full-slider.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/mystyle.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/jBox.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/youtube-video-gallery.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/youtube-video-gallery.css')); ?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <style type="text/css">
    
    .nav-tabs>li{
      float: none !important;
      display: inline-block;
    }

    .nav-tabs{
          display: inline-flex;
    }
  </style>

  </head>
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
          fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
  </script>
  <body>
         <?php echo $__env->make('include.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <header id="myCarousel" class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>

            <div class="carousel-inner">
                <?php echo $__env->yieldContent('slider'); ?>
            </div>

            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="icon-prev"></span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="icon-next"></span>
            </a>
        </header>

        <div class="container-fluid padding-left0 padding-right0">
            <?php echo $__env->yieldContent('why-study'); ?>
        </div>

        <div class="container-fluid about-us">
            <div class="container" align="center">
                <div class="row margin-bottom10">
                    <span class="about-text">About us</span>
                </div>
                <?php echo $__env->yieldContent('about-us'); ?>
                <a href="about_us_bn.php" class="btn btn-primary margin-top5P" >Read in Bangla</a>
            </div>
        </div>

        <div class="container-fluid msg-bg">
            <div class="container" align="center">
                <span class="msg-text">Message from Chief Patrom</span>
                <div class="row">
                    <div class="col-sm-4"></div>
                        <?php echo $__env->yieldContent('message-chairman'); ?>
                    <div class="col-sm-4"></div>
                </div>
                
                <div class="row">
                    <div class="col-sm-1"></div>
                    <?php echo $__env->yieldContent('message-vice-chairman'); ?>
                    <?php echo $__env->yieldContent('message-principle'); ?>
                    <div class="col-sm-2"></div>
                </div><br><br><br>
            </div>
        </div>

        <section class="home-life border-diag-bottom">
            <?php echo $__env->yieldContent('at-a-glance'); ?>
        </section>

        <section class="result-bg">
            <?php echo $__env->yieldContent('home-result'); ?>
        </section>

        <section>
            <?php echo $__env->yieldContent('home-notice'); ?>
        </section>

        <section>
            <?php echo $__env->yieldContent('home-news-events'); ?>
        </section>

        <section>
            @yeild('contact')
        </section>

        <footer>
            <?php echo $__env->make('include.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </footer>
    
    <script src="<?php echo e(asset('frontend/js/myjs.js')); ?>"></script>
    <script>
        $('#myCarousel').carousel({
            interval:   4000
        });
    </script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/TweenMax.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/plugins/ScrollToPlugin.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/plugins/EaselPlugin.min.js"></script>
  <script src="<?php echo e(asset('frontend/js/animate-scroll.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo e(asset('frontend/js/animate-scroll.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo e(asset('frontend/js/bootstrap-dropdownhover.min.js')); ?>"></script>

  <script type="text/javascript" src="<?php echo e(asset('frontend/js/css3-animate-it.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo e(asset('frontend/js/jBox-min.js')); ?>"></script>

  <script type="text/javascript" src="<?php echo e(asset('frontend/js/jquery.youtubevideogallery.js')); ?>"></script>

  <script type="text/javascript" src="<?php echo e(asset('frontend/js/myjs.js')); ?>"></script>

  <script type="text/javascript" src="<?php echo e(asset('frontend/js/npm.js')); ?>"></script>

 
  <script>
    $(document).foundation();
    $(document).animateScroll();

    var doc = document.documentElement;
    doc.setAttribute('data-useragent', navigator.userAgent);
  </script>
  <!-- Bootstrap Dropdown Hover JS -->

  </body>
</html>