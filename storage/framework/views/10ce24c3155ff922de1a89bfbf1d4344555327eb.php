<div class="container-fluid padding-left0 padding-right0">
    <div class="col-sm-6 slide-bottom-left" align="center" style="position: static !important;">
      <h2 class="h2" style="margin: 0px !important">Why Study at <span>IPSC</span></h2>
      <p class="txt-color">The institution offers a good number of co-curricular activities such as. B CC army (male and female), BNCC navy (male) B CC Air (Male and Female), Army (Junior) Holde Pakhi, Rover Scouts (Air, Boys, Girls) Cub Scout, Air Scout, Navy Scout, Band group.English and Debate, Music, Dance, Display, Football, Volleyball, Basketball. </p>
        <a class="btn btn-primary margin-top7" href="why_study_en.php">Read More</a>
    </div>

    <div class="col-sm-6 slide-bottom-right" style="position: static !important;float: right;color: #000;background: #003B6E;">
      <div class="col-sm-2 img-dedication"><img src="<?php echo e(asset('frontend/images/ipsc.png')); ?>" class="img-responsive"></div>
      <div class="col-sm-10">
        <h2 class="h2">Dedication</h2>
        <p class="txt-color">১৯৬৫ সালের জুন মাসে ময়নামতি পাবলিক স্কুল হিসাবে নামকরণ করা হয়।[৩] ১৯৬৬ সালে ২৮শে সেপ্টেম্বর দানবীর মির্জা আহমেদ ইস্পাহানীর আর্থিক সহায়তায় গ্রহণের পর প্রতিষ্ঠানটির নাম ইস্পাহানী পাবলিক স্কুল নাম করন করা হয়। পরে ১৯৭৫ সালের ২৮শে অক্টোবর প্রতিষ্ঠানটিতে কলেজ শাখা করার পর প্রতিষ্ঠানটির সর্বশেষ নাম করন করা হয় ইস্পাহানী পাবলিক স্কুল এন্ড কলেজ।</p>
        <a class="btn btn-primary margin-top7" href="details.php">Read More</a>
      </div>
    </div>
</div>